"""
CoCo at ICSE
"""
import random

def score2016_of(team):
    """Function Doc String"""
    return random.randint(1, (len(team)%3)+1)

COCOTEAM = ["Dominik", "Jan", "Julia", "Sophia"]
if len(COCOTEAM) <= 3:
    print "CoCo Team is not complete."

WINNER = True

COCO_SCORE = score2016_of(COCOTEAM)

if COCO_SCORE == WINNER:
    print str(COCOTEAM) + " are happy!"
else:
    print "We are still happy because "
    for coolPerson in COCOTEAM:
        print coolPerson
    print "will have a great time here."
