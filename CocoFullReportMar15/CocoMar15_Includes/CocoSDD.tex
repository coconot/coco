%\part{Software Design Description (SDD)}


\chapter{System Architectural Design}


\section{Overview}
CoCo includes a Best Practice Repository (BPR), an Error Tracking functionality (ET), a command line interface (CLI), and a Graphical User Interface (GUI). The BPR builds on the existing software Pylint, that allows to implement modular checkers for different code quality criteria. 
The standalone CLI integrates the functionalities of the BPR and ET and allows the use across platforms and without Integrated Development Environments (IDE). Our project's GUI builds on this CLI and is implemented as a plugin for the PyCharm IDE \cite{PycharmIDE}. %\footnote{PyCharm: \url{https://www.jetbrains.com/pycharm/}}.
Figure \ref{fig:components} illustrates how these components are organized into the CoCo system architecture. \\
%Table \ref{tab:requirements} gives an overview over software prerequisites for installing CoCo. Both CLI and GUI are not restricted to any platform.
\begin{figure}[h]
\centering
\includegraphics[width=0.7\columnwidth]{CocoPics/CoCoComponents.pdf}
\caption{CoCo components: Best Practice Repository (BPR), Error Tracking functionality (ET), command line interface (CLI), Graphical User Interface (GUI).}
\label{fig:components}
\end{figure}

%\begin{table}
%\centering
%\caption{Overview of requirements for the installation of CoCo}
%\begin{tabular}{lll}
%Software & Version & Component \\
%Python & 2.7 & all \\
%Git & ? & all \\
%PyCharm & ? & GUI \\
%\end{tabular}
%\label{tab:requirements}
%\end{table}

%SC: Structure Chart
%\section{Discussion of Alternative Designs }
%Not NInja


\section{Detailed Description of Components}\label{s:components}

\subsection{BPR: Pylint}\label{ss:bpr}
As a convenient solution matching the requirements of section \ref{ss:softwarereq}, we identified Pylint \cite{_pylint_????}.
%Pylint is written in Python and extensible by writing customized checking modules called \emph{Checker}.
%We introduced a new error category in Pylint named \emph{Beginner Errors} and set up several checkers to find mistakes commonly made by beginners. A detailed description of the checkers implemented can be found in section \ref{ss:bpr}.
Pylint is a Python module that implements various checkers for code quality. It allows to detect errors, coding standards violations, and code smells \cite{beck_refactoring:_2000}\cite{FowlerCodeSmell}.
For all detected items, it displays error messages \cite{PylintMessages} with their categories and collects statistics about these categories in reports. These reports include a comparison with the previous Pylint check of the same module. 
Pylint performs static code analysis, so that it can be run on code without actually running the code. It comes with a CLI, a GUI and a Python library. Dependent Python libraries are \texttt{astroid}\cite{Astroid} and \texttt{logilab-common}\cite{Logilab}. \\
For CoCo, we only make use of the Python library and the error messages to extend the coverage of error detection, and to integrate it into our own IDE plugin. We do not use the reports, but replace them with our error tracking module. 
The part of the library that defines the coverage are the checkers. These checkers are organized in a modular way, so that they are easily extendible. Each checker recognizes a set of errors by parsing the code either in a shallow string-based fashion or by building an abstract syntax tree (realized with the \texttt{astroid} module). Each checker comes with a specific test module, which serves for module tests. All errors are organized into categories \cite{PylintOutput} where the first letter serves as a category identifier in error codes:
\begin{itemize}
\item \textbf{R}efactor for a good practice metric violation
\item \textbf{C}onvention for coding standard violation
\item \textbf{W}arning for stylistic problems, or minor programming issues
\item \textbf{E}rror for important programming issues (i.e. most probably bug)
\item \textbf{F}atal for errors which prevented further processing
\end{itemize}
We added another category named ``\textbf{B}eginnerErrors'' that contains errors that novice programmers most typically produce or struggle with. This list of errors, shown in Table\,\ref{tab:beginnererrors}, was composed by going through relevant literature \cite{altadmri_37_2015}\cite{yarmish_revisiting_2007}\cite{mccall_meaningful_2014}, selecting errors relevant for Python beginners, and finally prioritizing with factors concerning severity and feasability of the implementation. Some of the errors identified in literature are already covered by Pylint, which is indicated by error codes other than "B" in \ref{tab:beginnererrors}. 
Note that this collection of typical beginner errors is designed to be extended from practical experience, so teachers using our CoCo in their classes can add frequently occurring or struggle causing errors to particularly be checked for. Of course these errors are limited to the types of errors that can be recognized without the knowledge of the purpose of the code, so logic errors cannot be found unless they are manifested in another type of error. \\
We modified the Pylint module in such a way that it still runs string-based checkers if the syntax tree cannot be built, such that other (string-level) errors can still be found if there is a syntax error in the code. In this way, we guarantee that the user will be notified about errors in the code that raise both syntax errors and string-based errors. This is especially necessary for programming beginners, since certain types of beginner errors result in syntax errors, but "error" as an error category is just not informative enough for a beginner to resolve this error. Note that a certain part of the code can cause several errors, so the user will receive notifications for each of these. \\
Since the range of error types is large, Pylint allows to configure the output for each user \cite{PylintMessageControl} with a "pylintrc" configuration file. For example, low-level errors like unrecommended variable names can be marked not to be reported, if the user is e.g. only annoyed by it.
% Together with our new checkers for beginner errors, we will also deliver several configuration files, each of them tailored towards a certain level of programming. In order to enable the users to control these configurations for themselves, we will include a detailed instruction how to do so.

\begin{table}
\centering
\caption{List of beginner errors in order of priority with CoCo Error Codes and short examples where possible.}
\resizebox{\columnwidth}{!}{
\begin{tabular}{l|lll}
Pylint Code & Type & Description & Example \\
\toprule
B0001 & Syntax & confusing assignment (=) with comparison operator (==) & \texttt{if a=b} \\
B0002 & Syntax & Getting greater than or equal/less than or equal wrong & \texttt{a => b} \\
B0003 & Syntax & Condition with \{ \} instead of () & \texttt{if \{a==b\}} \\
C0102 & Semantic & Using keywords as method or variable names & \texttt{i = dict} \\
C0102 & Semantic & Naming your modules after standard modules & \texttt{email.py} \\
B0004 & Semantic & Modifying a list while iterating over it & \\
B0005 & Syntax & Wrong separators in loops (Java,... style) & \texttt{for (i=0, i<N, i++):} \\
B0006 & Syntax & Forgetting parentheses after method call & \texttt{MyClass.myMethod} \\
B0007 & Semantic & Method with non-void return type is called and its return value ignored & \\
B0008 & Semantic & More than one exception type for except statement & \texttt{except ValueError, KeyError} \\
B0009 & Syntax & Starting with indentation in wrong column & \\
B0010 & Semantic & Use paths in import statements & \texttt{import mydir/mycode.py} \\
B0011 & Semantic & Control flow can reach end of non-void method without returning & \\
B0012 & Semantic & Forget to initialize counters, list accumulators & \texttt{i+=1} \\
B0013 & Syntax & Unbalanced parantheses/brackets/quotation marks & \texttt{while (a==b]} \\
\end{tabular}
}
\label{tab:beginnererrors}
\end{table}

\newpage
\subsection{ET: Error Tracking with a Database}\label{ss:et}

Besides the BPR, error tracking is one of the main aspects of how CoCo helps novice programmers. CoCo's error tracking component (ET) enables the users to follow their Personal Software Process (PSP): By inspecting the quantity of errors by type and code snippets that led to previous fixes of these errors, the users receive feedback on their progress in learning to program in Python. The ET component thus includes functionalities to:

\begin{itemize}
\item Recognize compiler errors as well as BPR messages.
\item Save statistics of these errors (e.g. the number of errors per type).
\item Compute code diffs between the erroneous state and the state when the error is fixed.
\item Add these diffs to a repository in order to improve the fixing time when a similar error occurs again.
\end{itemize}

As implemented in CoCo, the user has to initiate the error tracking process by either running the command line version of CoCo or by using the Coconut button or keyboard shortcut in the Pycharm plugin (GUI). This triggers a code checking process which collects Pylint messages, including compiler errors, convention violations and best practice violations from the BPR. CoCo collects all these messages and assigns them to the current code state.

CoCo keeps track of the current and previous code states by using an internal Git repository behind the scenes. In order not to interfere with potentially existing Git repositories the user uses to develop their code, CoCo manages internal Git repositories in a hidden \enquote{.coco} directory in the user's home directory. The directory contains subdirectories which match a hashed value of the project directory in order to manage multiple projects with CoCo. Each subdirectory contains a separate Git repository initiated by Coco. On every execution of the ET component, CoCo copies the current code files in the corresponding Git repository. This allows to easily keep track of errors and file changes and to compute diffs between the code states.

To keep track not only of file changes but also of error tracking information, a simple database should be used. Server based database management systems (as MySQL, PostgreSQL) do not meet the requirements described in the previous section, since they require an additional database set-up by the user. Instead we opt in for SQLite, a database management system which is already integrated in our choosen runtime environment Python 2.7.
SQLite doesn't require any separate database server processes and is thus a lightweight solution for our requirements.
It enables to query and update the Error Tracking database using SQL queries, a fact that simplifies the aggregation of error information and calculation of error statistics.
%An SQLite library is already included in a default Python 2.7 installation. 
The information which has to be saved in the database leads to the following table schema:

\begin{itemize}
\item id: auto-incremented entry ID
\item project: root directory of the current project in order to differentiate error tracking from different projects
\item error\_source: e.g. compiler message, PEP-8 violation, BPR...
\item error\_type: e.g. ImportError, SyntaxError, BeginnerError...
\item error\_number: internal ID of the error message
\item error\_message: the error message
\item date\_occured: date and time when the error occured
\item date\_fixed: date and time when the error didn't occur anymore
\item diff\_id: ID for a table join to access changed files which links to:
\begin{itemize}
\item file\_name: file name from within the project root; to be found in the internal Git repository
\item file\_diff: \enquote{git diff} between the erroneous and the fixed state
\end{itemize}
\end{itemize}

To display information from this database (e.g. recent diffs that lead to the fix of an error the user does again), the CoCo ET module simply has to query the SQLite database for the error's characteristics (including error type and error id). The CoCo CLI and the CoCo GUI then show code diffs matching to the current occuring errors (e.g. \enquote{Last time this error occured, you fixed it as follows...}).

\subsection{CLI: Combining BPR and ET}
The core components BPR and ET are integrated in the CLI, which allows to access error messages and track them from a command line terminal. The input to BPR is the Python script to analyze, the output is the combination of the error messages and a report. The ET further processes this input and presents a final report to the user, as described in \ref{ss:et}. The final report includes the following sections: (1) Errors found with line numbers, error type, relevant line (from BPR), (1a) a pointer to the position in the code, (1b) previous fixes with code diffs in Git style (from ET), and (2) a summary with the list of errors fixed and potential warnings or comments.
%CoCo stores your errors in a sqlite database in the hidden directory \texttt{\url{.coco}} in your home directory. In case you want to inspect the database, e.g. with an sqlite browser, open the \texttt{\url{\~/.coco/tracking.db}} file.\\ 
For using the CLI, Python 2.7 and Git are required. Python module dependencies are resolved by delivering the modules logilab-common (v1.0.2) and astroid (v1.3.7) directly with the CLI. In order to run the CLI, no installation is needed after the code is unpacked and added to the system path.\\
The CLI (\texttt{coco.py}) is called with the code file to be analyzed as argument: \texttt{python coco.py mycode.py}. 
Examples of the output of the CLI are presented in \ref{lst:lastfix} (linking to previous fixes) and \ref{lst:noerrors} (no code smells).
%\begin{itemize}
%\begin{lstlisting}[basicstyle=\tiny]
%pylint mycode.py 
%\end{lstlisting}
%\texttt{coco.py} input: Pylint output, output: error messages, previous fixes, list of errors fixed since last run \ref{lst:lastfix} \ref{lst:noerrors}

\newpage
\begin{lstlisting}[caption={Extract from the CLI output: a BeginnerError (B0001) is detected, a previous fix for this error type is reported},label={lst:lastfix},basicstyle=\tiny,captionpos=b]
 9:0 B0001 Use "==" for comparison
    if topping = "tuna": #B001

               ^

    This is how you fixed this error the last times:
    SOLUTION #0 --------------------------------
      @@ -17,7 +17,7 @@ def orderPizza(topping="nothing", size=30):
           print "Your pizza (d=%dcm) with topping %s was ordered" % (size, topping)
           print "Delivery in %d minutes" % (random.randint(0,100))
       
      -if __name__ = "__main__":
      +if __name__ == "__main__":
       
           orderPizza("cheese", 40)
           orderPizza("bolognese", 70)
\end{lstlisting}

\begin{lstlisting}[caption={Extract from the CLI output: list of errors fixed since last run of CoCo, no errors left},label={lst:noerrors},basicstyle=\tiny,captionpos=b]
CoCo Prototype
Errors found:
  Kewl! No errors!
Errors fixed:
  E0001
  B0001
\end{lstlisting}

%\begin{figure}
%\centering
%\begin{minipage}{.5\textwidth}
%  \centering
%  \includegraphics[width=\columnwidth]{../CocoPics/lastfix.png} 
%  \caption{Extract from the CLI output: a BeginnerError (B0001) is detected, a previous fix for this error type is reported}
%  \label{fig:lastfix}
%\end{minipage}%
%\begin{minipage}{.5\textwidth}
%  \centering
%  \includegraphics[width=\columnwidth]{../CocoPics/noerrors.png} 
%  \caption{Extract from the CLI output: list of errors fixed since last run of CoCo, no errors left}
%  \label{fig:noerrors}
%\end{minipage}
%\end{figure}


\subsection{GUI: IDE Plugin}\label{sec:GUI}
In Figure \ref{fig:GUIs} the current state of the PyCharm IDE plugin GUI is shown in comparison to the state as it was in the summary report. 
In GUI Version 2.0 we improved the layout of the windows so that all contents that belong to CoCo are on the right, in the window named Coco Interface. 

\paragraph*{CoCo Interface} This is now the only window for CoCo. It is used to represent
detected issues in the code in an appealing way. To represent the issues in a clear form,
we introduced tabs that split the issues into categories. If an issue occurs in a tab, the
tab is labelled with the number of errors found in that category. Beside the occurred issue, also a
highlighted hint for a possible solution is contained in the window below a tab. 
Another new feature includes a hyperlink at the very bottom of the error message, that, when clicked, automatically opens the browser and navigates to a search of that topic on stackoverflow.com. 
 If there are more issues found within one tab, the issues are separated by a horizontal line.
Color-coded highlighting of CoCo's output was achieved via integrating HTML and now aids the users in quickly finding the information relevant to them. 



\begin{figure}[h!]
\centering
\begin{subfigure}{.95\textwidth}
	\centering
	\includegraphics[width=0.95\columnwidth]{../CocoPics/CoCoScreen20160106.png}
	\subcaption{\textbf{GUI Version 1.0} January 6, 2016.}
	\label{fig:GUI1}
\end{subfigure}
\quad
\begin{subfigure}{.95\textwidth}
	\centering
	\includegraphics[width=0.95\columnwidth]{../CocoPics/CoCoScreen20160316.jpg} 
	\subcaption{\textbf{GUI Version 2.0} March 16, 2016.}
	\label{fig:GUI2}
\end{subfigure}
\caption{\textbf{The CoCo Plugin GUI.}}
\label{fig:GUIs}
\end{figure}


\paragraph*{CoCo Tree} (Moved into CoCo Interface) Contains a coconut tree which realizes aspects of gamification. The image shown depends on the status of the user's error tracking history. We realized the idea of a coconut falling to the ground every time an issue is fixed by calling on different gif animations. 
\paragraph*{CoCo Plain Output} (Removed) This window was used to show the plain output PyCharm receives
from the CoCo CLI. It can be helpful for debugging issues, but was removed for GUI Version 2.0 because the information is redundant with that shown in CoCo Interface.

\paragraph{Tools menu and CoCo button} A button to start CoCo is contained in the Tools menu. The button is recognizable by showing the icon that is also displayed on the front page of this report. Another button
nearby the Python editor is also integrated in the form of the icon that appears in the coding area. A great usability improvement in Version 2.0 is that the coding file does not need to be saved manually before starting CoCo, but that this step is included when CoCo is run by any of the three means (Tools menu, keyboard shortcut or coding area button). 



\paragraph{Internals} To get an overview of how the CoCo IDE Plugin internally works, have a look at the sequence diagram in Figure \ref{fig:sequencediagram} and the
class diagram in Figure \ref{fig:classdiagram}.

\newpage

\begin{figure}[!h]
\centering
 \includegraphics[width=0.9\columnwidth]{../CocoPics/CoCoSequence.pdf}
	\caption{This sequence diagram represents the actions which are performed inside the CoCo IDE Plugin if CoCo is called from the IDE. 
	Note that this sequence represents the first call of CoCo. In any further calls the objects CoCoAccess, CoCoResourceManager and CoCoUIWindow
	will not be created again.}
	\label{fig:sequencediagram}
\end{figure}

\newpage

\begin{figure}[h]
\centering
 \includegraphics[width=0.9\columnwidth]{../CocoPics/CoCoUML1.pdf}
	\caption{This class diagram shows the internal structure of the CoCo IDE Plugin. The RunCoCoAction class is the entry point to the plugin and is called by
	the IDE if the CoCo Button in the IDE is pushed. We use an interface (ICoco) to achieve an encapsulation of the plugin functionalities. CoCoAccess implements
	the functions defined in ICoCo. We use the CoCoRessourceManager to maintain all resources like images and icons at one single point. The CoCoUIWindow class
	implements the UI-Window which is displayed in the IDE. MyHyperlinkAdapter is used to open links which are shown in some messages on the CoCoUIWindow.}
	\label{fig:classdiagram}
\end{figure}


\newpage
\section{Requirements Traceability Matrix}\label{sec:ReqTrace}

Table\,\ref{tab:reqtrace} provides a table that shows where each requirement named above under user interface requirements and software system attributes is supported in the design components. The requirements listed under software interface requirements are not further mentioned here because they were fulfilled by the choice of software. Some of the requirements cannot be checked as done at the current status, since they require a user evaluation. For example, it needs to be evaluated in practice, whether the user is irritated by the appearance of the palm tree. 

%``The requirements traceability matrix is a grid that links product requirements from their origin to the deliverables that satisfy them.'' \cite{project_management_institute_guide_2013}% PMBOK Guide 
%
%% IEEE Supplement:
%Provide a matrix showing where each feature identified in the SRS is
%supported by the design components.
%
%If you are developing the software in multiple increments, then a
%traceability matrix should be produced for each version.


\newcommand{\tracecolw}{3cm}
\begin{table}[ht]
\captionof{table}{\textbf{Requirements Traceability Matrix.} For CoCo GUI Version 2.0, March 14, 2016. Blue checkmarks show additions since Version 1.0. }
\label{tab:reqtrace}
\centering
\begin{tabular}{l|cccc} %p{5cm}p{\tracecolw}p{\tracecolw}p{\tracecolw}} %p{3cm}p{11cm}}
%Requirement & Best Practice Repository (BPR) & Error Tracking (ET) & Command Line Interface (CLI) & Graphical User Interface (GUI)  \\
Requirement & BPR & ET & CLI & GUI  \\
\midrule
Lines &   & \checkmark & \checkmark & \checkmark\\
Nature &   &  &  \checkmark  &  \checkmark  \\
Links &    &  &     &  \checkmark  \\
Assistant &    &   &   &   \checkmark \\
Metrics &   & \checkmark &     &  \checkmarkcolored \\
Compile &    &  &  \checkmark   &  \checkmark  \\
%human interface guidelines
Interact &    &   &    & \checkmarkcolored \\
Configure &    &  & \checkmarkcolored &  \checkmarkcolored \\
Understand &   &  &  & \checkmarkcolored  \\
Irritation &    &  &      &  \\
Similarity &    &  &      &  \checkmark\\
Colors &   &  &      &   \checkmark \\
%own experience
Motivation &    &  &     &  \checkmark  \\
Structure &     &   &    \checkmark  &   \checkmark  \\
Progress &     &  \checkmark &      &  \checkmarkcolored   \\
%\hrule
%----------------------------------------------------------
		&		&		&     &   \\
Portability &  \checkmark   & \checkmark &   \checkmark  &  \checkmark   \\
Maintainability &  \checkmark   &   &       &\\
Performance &     & \checkmark  & \checkmark     & \checkmarkcolored \\
Security &     &   \checkmark&        &\\
Emotional Factors &     &  &      &  \checkmark  \\
Usability &     &    &      &  \checkmarkcolored  \\
\end{tabular}
\end{table}








%\chapter{User Interface Design}
%\section{Description of the User Interface}
%\subsection{Screen Images}
%\subsection{Objects and Actions}
%
%\chapter{Additional Material}
