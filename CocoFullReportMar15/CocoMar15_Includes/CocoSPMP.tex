\newpage

%\part{Software Project Management Plan (SPMP)}

\addchap{Preface}

This full report is an extended version of the summary report that was submitted on January 18, 2016. 
We quickly want to summarize what has changed to simplify the reader's navigation.

First of all, several user stories were added to describe the potential usage of our Code Coach CoCo in a more appealing and detailed manner (Section\,\ref{sec:Userstories}). 

Secondly, we worked a lot on the graphical user interface that integrates CoCo into the IDE PyCharm. We describe these new or updated features, also showing screenshots. 
In addition, a class diagram and a sequence diagram are included that give insight into the structure of our source code (Section\,\ref{sec:GUI}). 

These features are added as new test cases and the requirements traceability matrix was also updated accordingly, highlighting improvements (Sections\,\ref{sec:ReqTrace} and \ref{sec:Testcases}). \\
\\
Finally, we would like to share a general observation that we made. We think that naming our project CoCo at the beginning was one of the best decisions we made that helped us grow together and develop a real team spirit. Coconut-milkshakes, coconut chocolate bars and songs involving the word ``coco''  quickly became part of our team meetings. And of course the idea for the palm tree animation, which we consider an essential part of our Code Coach motivational strategy, was born from the name. It is almost reminiscent of the story of the ``black team'' (a testing team whose members started to dress all in black and really live their reputation) whose story is told by Tom DeMarco and Timothy Lister in their book ''Peopleware'' \cite{demarco_peopleware_2013}: ``[...] the silly exaggerated behavior were part of the fun, but there was something much more fundamental going on. The chemistry within the group had become an end in itself.''


\chapter{Introduction and Project Organization}
\section{Project Overview}

% Abstract as registered
This project is oriented towards a participation in the SCORE contest 2016. In particular, we implement a solution for the ``CodeCoach'' project task supervised by Dr. Claudia Szabo \cite{szabo_score_????}. Our code coach CoCo supports novice programmers, such as first-year computer science students, in learning to write clean code. It focuses on creating awareness and motivation for code readability and for adhering to best practices. In this way, we aim to avoid frustration due to extensive debugging and error identification efforts in later phases of the students’ programming process. 

%Since it is widely used as a beginner's first programming language, we implement CoCo to support Python. CoCo includes a Best Practice Repository (BPR), an Error Tracking functionality (ET) and an integrated Graphical User Interface (GUI). The BPR builds on the existing software Pylint, that allows to implement modular checkers for different code quality criteria. To the existing Pylint checkers we add checkers specifically focusing on beginner errors and adapted to display messages that explain the best practice violations in a beginner-friendly manner. Furthermore, CoCo enables the users to follow their Personal Software Process (PSP) via the ET component: by inspecting the quantity of errors by type and code snippets that led to previous fixes of these error, the users receive feedback on their progress in learning to program in Python.
%A standalone command line interface (CLI) integrates the functionalities of the BPR and ET and allows the use across platforms and without Integrated Development Environments (IDE). Our project GUI builds on this CLI and is implemented as a plugin for the PyCharm IDE. Striving for a personalized coach-student interaction, we apply gamification techniques, including motivational visualizations of the coding progress.

The time planned for us to work on the CoCo project spans over roughly one year. Throughout this time the team has expanded, worked hard and learned quite a lot about software engineering as individuals and as a group. In the following, we report the development process, our experiences and the resulting software CoCo. 


%\section{Project Deliverables - Sophia}  % For full report better because we're not turning in any files for summary
%
%\begin{table}[htb]
%\captionof{table}{\textbf{Project Deliverables.}}
%\label{tab:deliverables}
%\centering
%\begin{tabular}{p{5cm}p{3.5cm}p{5cm}}
%Name  & Delivery Date  & File/Folder Name\\
%\midrule
%Project Summary &  2016-01-15 &  CoCoSummary.pdf\\
%Pylint for Coco &  2016-01-15 & pylint4coco   \\
%Coco Plugin for Pycharm & 2016-01-15 &  \\
%Bitbucket Repository & 2016-01-15 &  \\
%OpenProject Backlog & 2016-01-15 &  \\
%\end{tabular}
%\end{table}

%
%\begin{figure}[h]
%	\centering
%	\includegraphics[width=.9\textwidth,height=.35\textheight]
%	{20151207_BitbucketSource.png}
%	\caption{\textbf{Bitbucket Source.}}
%	\label{fig:BbSource}
%\end{figure}




%\chapter{Project Organization}
\section{Software Process Model}

A software process model describes and formalizes the phases and activities in a software development process. Literature proposes a large variety of software process models since requirements and settings among projects often differ to a large extent. To choose a suitable and useful software process model, the following distinctive features of our team have to be considered:

\begin{enumerate}
\item As students, we cannot work full-time for the project. Each of us has a different timetable, such that regular team meetings with all members have to be reduced to a single time slot per week. This restricts concepts of some software process models, e.g. we cannot implement \emph{Daily Scrum} meetings as proposed by the Scrum software development model.
\item The time available for the project also differs weekly for each member, since exams, presentations and other events concerning our studies can often not be planned a long time in advance. This limits the usefulness of software process models that require a large amount of resources planning.
\item At the university, we have direct access to our \enquote{customers}, i.e. beginner programmers. This makes requirements engineering and prototype testing easy.
\end{enumerate}

Because of these requirements, we opt for an \emph{Iterative Enhancement Model} \cite{IEModel}.
This model builds on a \emph{Prototyping Model} \cite{SoftwarePrototyping}.
The model implies an incremental process, starting with an early prototype that gets improved in every incremental step. Since we decided to meet regularly every week, we set the length of an iteration to one week. Every week, a functional prototype should be available, ideally implementing improvements since the previous prototype.

To overview the process, we created and update a project control list \cite{IEModel}.
The project control list contains all features and tasks that have to be implemented until we reach the final system. The list and responsibilities for each task gets managed in an OpenProject system \cite{OpenProject},
which offers a broad view on the process we made so far as well as on upcoming tasks.

\section{Tools and Techniques}


\begin{table}[htb]
\captionof{table}{\textbf{Tools used for CoCo development.}}
\label{tab:tools}
\centering
\begin{tabular}{p{4cm}p{10cm}}
Tool  & Function  \\
\midrule
Zotero &  literature, research administration\\
Git &  version control of own source code and project report \\
Mercurial & version control for Python package dependencies \\
Bitbucket & online code repository, Wiki documentation \\
OpenProject & software development process tracking \\
Google Drive & creation of slides and diagrams \\
IntelliJ IDEA & IDE for Java development \\
PyCharm & IDE for Python development \\
\end{tabular}
\end{table}



Table \ref{tab:tools} presents an overview of tools used during CoCo development. 
Especially in the planning phase, we used Zotero for organizing the literature that we reviewed. By sharing our notes about individually read papers in a Zotero group, everybody had access to all the insights and the sources. After broad reviews we summarized the findings that were relevant to our practical application in Wiki pages of our CoCo Bitbucket repository, as can be seen in     Figure\,\ref{fig:BbWiki}.
 In this way, we brought code and concept close together. 



The CoCo Bitbucket repository was only visible to the project members during development, but is now publicly available for the participation in the contest \cite{CoCoBitbucket}. In the repository we organized both integrated modules like Pylint, our own code, and our documentation and report to intermesh seamlessly. 
For version control we used both Git and Mercurial. The development of our code is version-controlled with Git, but in order to integrate external Python modules in their latest version, we had to use Mercurial to pull the latest changes. % but for deployment we're not updating, right? because the updates crashed pylint?
As motivated in the previous section, we decided to use the OpenProject platform to document and organize our software development process. OpenProject allows to structure the project into several work packages and versions, containing tasks and subtasks which can be assigned to project members, and to track the progress in the overall development. As an example, Figure\,\ref{fig:OPTasks} shows the OpenProject taskboard of CoCo Version 1.0.
In addition to internal documentation and presentations, we also held presentations in two seminars at university to present our ideas and to demonstrate our prototypes. For that purpose, we found Google Drive presentations and drawings most suitable, because they allow to collaboratively work on slides and illustrations. 
We developed code in both Java and Python: Python for the implementation of the CoCo modules CLI, BPR and ET (see Section \ref{s:components} for a description of the modules),
and Java for the development of the PyCharm plugin. For Python coding we used the PyCharm IDE, for Java IntelliJ IDEA.

%TODO also techniques like brainstorming, paper mock-ups, pair programming etc.? 





\begin{figure}[h!]
	\centering
	\includegraphics[width=1\textwidth,height=0.95\textheight]
	{taskboard-gui-07-12-15.png}
	\caption{\textbf{OpenProject Backlog Taskboard.} Screenshot taken on December 7, 2015.}
	\label{fig:OPTasks}
\end{figure}











\section{Roles and Responsibilities}

\begin{table}[htb]
\captionof{table}{\textbf{Roles and Responsibilities.}}
\label{tab:roles}
\centering
\begin{tabular}{p{3cm}p{11cm}}
Name  & Role  \\
\midrule
Julia Kreutzer & Best Practice Repository Manager, Documentation Assistant  \\
Jan Pawellek &  Error Tracking Manager, PyCharm Plugin Assistant \\
Sophia Stahl & Documentation Manager, Best Practice Repository Assistant  \\
Dominik Sterk & PyCharm Plugin Manager, Error Tracking Assistant   \\
\end{tabular}
\end{table}

By having weekly meetings and distributing the tasks within the team, we ensure that each team member equally contributes to the project. A screenshot of a part of the commit history in Bitbucket can be seen in Figure\,\ref{fig:BbCommits}, demonstrating our way of working together. During the first months, the team consisted of the three students Jan, Julia and Sophia, for the second semester a fourth student, Dominik, joined the project. 
Table\,\ref{tab:roles} shows a rough outline of the roles and responsibilities that each team member has. Besides maintaining one main responsibility, each student also assists another student with a second major topic. However, we often made the experience that problems got solved more quickly when we all were involved and discussed the problem at hand at our meeting, since the different perspectives quickly led to a solution. 


\begin{figure}[h!]
	\centering
	\includegraphics[width=.8\textwidth,height=.35\textheight]
	{20160110_BitbucketWiki.png}
	\caption{\textbf{Bitbucket Wiki.} Screenshot taken on January 10, 2016.}
	\label{fig:BbWiki}
\end{figure}


\begin{figure}[h!]
	\centering
	\includegraphics[width=.9\textwidth,height=.4\textheight]
	{20151207_BitbucketCommits.png}
	\caption{\textbf{Bitbucket Commits.} Screenshot taken on December 7, 2015. }
	\label{fig:BbCommits}
\end{figure}

\section{Time Schedule}
Within the first weeks of the project, we devised a time schedule for the different parts of the project. Figure\,\ref{fig:Gantt1} displays this schedule in the form of a Gantt chart, where horizontal bars represent the time periods during which the respective part of the project would be dealt with \cite{project_management_institute_guide_2013}. To what extent this time schedule was fulfilled will be discussed below in Section\,\ref{sec:LessonsLearned}, where Figure\,\ref{fig:Gantt1} can be found to simplify the comparison to the actual schedule.

We defined five main parts of the project. (1) The first part consists of planning activities, such as brainstorming, which is always linked to the requirements analysis, the research on and compilation of best practices for the BPR, and exploration of the Pylint program. 
The next three parts cover the actual development: After having created a (2) prototype that works on the command line, the next step is to transfer the main functionalities into a (3) first GUI version. Following that, all experiences made so far are taken into account for developing the (4) product. At the beginning of each of these phases, a brainstorming session and requirements analysis are to be performed. 
Within our software process model of iterative enhancement, each development phase can be regarded as one major iteration, achieved by many more ``sub-increments''. 
(5) The fifth part consists of other non-coding activities such as the documentation, meetings with our supervisors, and presentations of our project at various occasions. 
% in person with our professor in Heidelberg as well as via Skype with Claudia Szabo


%\begin{sidewaysfigure}[h]
%	\centering
%\begin{ganttchart}[
%  vgrid,
%  canvas/.style={fill=none, draw=black!5, line width=.5pt},
%  y unit chart = 0.6cm,
%  y unit title = 0.4cm,
%  title height=1,
%  today = 2,
%  today label = { Current Week},
%  bar label font=\small,
%  milestone inline label node/.append style={font=\tiny}
%  ]{1}{36}
%\gantttitle{Weeks}{36} \\
%\gantttitle{Apr}{3} % 16-18
%\gantttitle{May}{4} % 19-22
%\gantttitle{Jun}{4} % 23-26
%\gantttitle{Jul}{5} % 27-31
%\gantttitle{Aug}{4} % 32-35
%%\gantttitle{Sep}{4} % 36-39 Summerbreak
%\gantttitle{Oct}{5} % 40-44
%\gantttitle{Nov}{4} % 45-48
%\gantttitle{Dec}{5} % 49-53
%\gantttitle{Jan}{2}\\ % 1-2
%\gantttitlelist{16,...,35}{1}
%\gantttitlelist{40,...,53,1,2}{1}\\
%%\gantttitlelist{1,...,36}{1}\\
%
%%\ganttgroup{ Planning}{1}{16} \\
%\ganttbar[progress=0, progress label text={}]{ Brainstorming}{1}{3} 
%\ganttbar[progress=0, progress label text={}]{}{7}{8}
%\ganttbar[progress=0, progress label text={}]{}{14}{15}
%\ganttbar[progress=0, progress label text={}]{}{21}{22}\\
%\ganttbar[progress=0, progress label text={}]{ Requirements Analysis}{2}{4} 
%\ganttbar[progress=0, progress label text={}]{}{8}{9}
%\ganttbar[progress=0, progress label text={}]{}{15}{16}
%\ganttbar[progress=0, progress label text={}]{}{22}{23}\\
%\ganttbar[progress=0, progress label text={}]{ BPR Compilation}{3}{6} \\
%
%%\ganttgroup{ Exploration}{1}{16} \\
%\ganttbar[progress=0, progress label text={}]{ Explore Pylint}{3}{6} \\
%
%
%\ganttgroup{ Dev: CLI Prototype}{7}{13} \\
%\ganttbar[progress=0, progress label text={}]{ BPR Implementation}{7}{13} \\
%\ganttbar[progress=0, progress label text={}]{ Error Tracking}{7}{13} \\
%
%
%\ganttgroup{ Dev: GUI Version 1.0}{14}{18} \\
%\ganttbar[progress=0, progress label text={}]{ Explore IDES}{14}{16} \\ %Ninja, Eclipse, Pycharm
%
%
%
%\ganttgroup{ Dev: Product}{21}{36} \\
%\ganttbar[progress=0, progress label text={}]{ GUI}{21}{36} \\
%\ganttbar[progress=0, progress label text={}]{ Checker}{21}{36} \\
%\ganttbar[progress=0, progress label text={}]{ Testing}{21}{36} \\
%%\ganttbar[progress=0, progress label text={}]{ Deployment}{25}{36} \\
%
%
%
%\ganttbar[progress=0, progress label text={}]{ Documentation}{1}{36}
%\ganttmilestone[inline, milestone label inline anchor/.style=below]{ Expose}{5}
%\ganttmilestone[inline, milestone label inline anchor/.style=below]{ Abstract}{29}
%\ganttmilestone[inline, milestone label inline anchor/.style=below]{ Summary Report}{36}\\
%
%% First call with Artur was actually on March 4, week 10
%
%\ganttbar[progress=0, progress label text={}]{Supervisor Meetings}{2}{2}
%\ganttbar[progress=0, progress label text={}]{}{7}{7}
%\ganttbar[progress=0, progress label text={}]{}{12}{12}
%\ganttbar[progress=0, progress label text={}]{}{17}{17}
%\ganttbar[progress=0, progress label text={}]{}{23}{23}
%\ganttbar[progress=0, progress label text={}]{}{28}{28}
%\ganttbar[progress=0, progress label text={}]{}{32}{32}
%\ganttbar[progress=0, progress label text={}]{}{36}{36}\\
%
%%\ganttmilestone[inline, milestone label inline anchor/.style=below]{Clean Code Seminar}{12}
%
%\ganttbar[progress=0, progress label text={\tiny Clean Code Seminar}]{Presentations}{12}{12}
%%\ganttbar[inline, progress=0, progress label text={\tiny Seminar CLI}]{}{22}{22}
%%\ganttbar[inline, progress=0, progress label text={\tiny Seminar GUI}]{}{32}{32}\\
%
%
%\end{ganttchart}
%\caption{\textbf{Time Schedule as Planned.} Gantt chart that displays the different phases
%of the project and the time periods in which they were planned to be worked on. Dev: Development.}
%% Bar color code: proportion white:grey corresponds to the progress of the respective phase (white = proportion done).}
%\label{fig:Gantt1}
%\end{sidewaysfigure}

