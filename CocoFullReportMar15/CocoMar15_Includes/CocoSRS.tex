%\part{Software Requirements Specifications (SRS)}

\chapter{Specific Requirements}

\section{External Interface Requirements}
\subsection{User Interfaces} \label{sec:Userstories}
The requirements for the user interface originate from four different sources: (1) The SCORE project description, (2) the team members' own experience, brainstorming and discussion sessions, (3) the user scenarios and derived user stories, (4) general human interface guidelines \cite{benyon_designing_2010}. Table\,\ref{tab:uireq} lists these requirements with names and short descriptions. Names are used to uniquely identify requirements for later references. The product features that are derived from these requirements are presented in Table \,\ref{tab:uifeat}.


\paragraph{Personas and user scenarios} Two possible ways to write user stories are personas, which describe a specific person and their situation in a detailed manner, and a more general tabular form of user scenarios that always cover only one specific aspect. We tried both techniques and found that both are helpful in different ways. The following two personas describe and illustrate a possible interaction of prototypical users with our CoCo system. Such personas are especially useful for really slipping into the shoes of a user. Another way of writing user stories is a tabular format \cite{Mountaingoat}, so some additional user stories are listed Table\,\ref{tab:userstories}. This approach was rather like drafting a requirements checklist. \\
\\
\textbf{Simon}, 18
\begin{itemize}
\item First term computer science student 
\item In the middle of the first programming course (Python)
\item No previous coding experience
\item Uses PyCharm Edu (as taught in the course) with Windows
\item Has to do weekly programming exercises
\item Teacher recommended him to use CoCo
\item By using CoCo he wants to quickly learn to avoid typical beginner errors
\begin{enumerate}
\item He opens PyCharm Edu (with CoCo plugin) and opens the Python script he has been working on for this week’s programming exercise. 
\item In order to recall where the code contained problems, he runs the script with the Python interpreter. An error message (NameError) is returned, the code does not run as intended. He remembers that he had problems with a counting variable.
\item To find out whether his code is free from beginner and pylint errors, he clicks on the CoCo button to run a CoCo check.
\item In the CoCo Interface window, he finds that there is one beginner error present in his code (plus two convention errors). He reads the explanation and learns that he forgot to initialize his counter variable before incrementing. 
\item He initializes the variable and again runs CoCo. Seeing a coconut fall from the CoCo palmtree, he realizes that he just fixed one of the errors. 
\item He continues to fix the convention errors with the help of the description and the pointers to the lines in his code. 
\item Finally, he tries to run the script again and he is happy to see that it runs without errors.
\end{enumerate}
\end{itemize}

\textbf{Anna}, 20
\begin{itemize}
\item First term computer science student
\item In the middle of the first programming course (Python)
\item Learned coding with Java at high school, also prefers to interact with the terminal over working with graphic user interfaces
\item Uses CoCo to check her programming exercises, she wants to track the development of her code and strives towards quickly writing clean code
\begin{enumerate}
\item Anna opens the terminal and creates a new file for the new programming exercise.
\item She starts writing the first lines with basic functions that she will need for the task. She wants to track the progress with the task, so she runs CoCo on the code from the command line.
\item CoCo reports one beginner error and from the description of the error she learns that this is a common error for Java programmers who are moving to Python (using Java-like for-loop declarations). Taking a look into the notes she took during the course, she remembers how to write the for-loop in proper Python style and runs CoCo again. No errors left!
\item She continues with the next bits of her code, running CoCo every time she has finished one function definition.
\item Again, she finds a for-loop error. This time, she gets suggestions how to fix it the way she has previously fixed it. Quickly she fixes this error as well. 
\item Once she is sure that the code solves the problem described in the programming exercise, she runs CoCo once again and finds only convention errors that are quickly fixed by adding spaces before brackets. Anna is happy that she was quickly able to make her code work and nicely readable.
\end{enumerate}
\end{itemize}



\begin{table}[h!]
\centering
\caption{\textbf{User stories.}}
\begin{tabularx}{\linewidth}{X|X|X}
As a/an & I want to … & so that… \\ 
\hline
beginner programmer & learn to program in python & I can manage all the tasks that I get as a computer science student \\ 
beginner programmer & know which errors I do often & I can prevent them in future exercises \\ 
beginner programmer & be able to tell from an error message what is wrong with my code & I can easily progress with the exercise \\ 
beginner programmer & get a hint for a solution to my error & I can progress quickly \\ 
beginner programmer & learn to write clean code right from the beginning & I don‘t adapt bad habits due to short-sighted laziness \\ 
beginner programmer & see my progress visually & I have the feeling of having achieved something \\ 
beginner programmer & read error messages that use my beginner vocabulary & I don‘t get lost in abstract explanations of simple concepts \\ 
beginner programmer & find out more about an error I did & I read about ways to approach preventing or fixing it \\ 
power user & recognize the environment where I program & I easily find information that I need for fixing an error \\ 
power user & use a simple keyboard shortcut to launch the program & I don‘t waste time starting the program but can focus on programming \\ 
 &  &  \\ 
experienced programmer & get rid of bad coding habits & I can write readable code that meets coding standards \\ 
 &  &  \\ 
Programming teacher & know common errors of my students & I know what to explain in more detail or practice more with them \\ 
\end{tabularx}
\label{tab:userstories}
\end{table}



\begin{table}
\centering
\caption{List of requirements for the user interface.}
\resizebox{0.5\columnwidth}{!}{
\begin{tabular}{l|l}
Name & Description \\
\midrule
%project description
Lines &  Find information about the lines with errors offending the best practices \\
Nature &  Learn about the nature of the bad practice error\\
Links &  Find links to resources describing how this practice can be remedied \\
Assistant &  Understand CodeCoach as coding assistant, similar to the Microsoft Office PaperClip \\
Metrics &  Inspect metrics recorded similar to those included in the Personal Software Process \\
Compile &  Being notified if compile errors are present \\
%human interface guidelines
Interact &  Intuitively find how to interact with the CodeCoach (start, end, read output) \\
Configure &  Find instruction how to configure CodeCoach \\
Understand &  Understand CodeCoach output easily\\
Irritation &  Be not irritated by the CodeCoach avatar \\
Similarity &  Similar functionalities organized in similar ways (mental model)\\
Colors &  Use colors appropriately and consistently \\
%own experience
Motivation &  Get motivated by the CodeCoach avatar \\
Structure &  Getting presented errors in a structured way \\
Progress &  Getting feedback about the progress made during coding \\
\end{tabular}
}
\label{tab:uireq}
\end{table}

\begin{table}
\centering
\caption{Product features for the user interface, derived from user interface requirements.}
\resizebox{0.4\columnwidth}{!}{
\begin{tabular}{l|l}
Feature ID & Description \\
\midrule
1 & Display of erroneous line \\
2 & Display of error category \\
3 & Display of links to further resources \\
4 & Display of avatar \\
5 & Undisturbing animation of avatar \\
6 & Colors which fit into color scheme of IDE \\
7 & Easy access to CodeCoach windows from IDE \\
8 & Integrate functionalities in a PyCharm style \\
9 & Getting helpful and precise error messages \\
10 & Getting presented relevant code examples \\
11 & Similar errors grouped together \\
12 & Display of a per-document error report \\
13 & Coloured highlighting of the source of the errors \\
14 & Visualized progress of the coding session \\
15 & Separate windows for animation and code  \\
16 & Option to hide CodeCoach windows \\
17 & Option to display CodeCoach windows \\
18 & Option to rescale CodeCoach windows \\
19 & Option to start CodeCoach analysis \\
20 & Option to end CodeCoach analysis \\
21 & Recognizable design of the avatar \\
\end{tabular}
}
\label{tab:uifeat}
\end{table}

%\begin{itemize}
%\item human-centered design, PACT framework \cite{benyon_designing_2010} helps us to identify requirements
%\item \textbf{People} Novice computer science students are our target group. They are in their fist year at university and have already gained knowledge about the general principles of programming. In particular, they have attended a first introduction to programming course, where they started to write their first code. Their programming process is guided by teachers, but they have to find solutions to programming exercises on their own or with the help of fellow students. Issues due to physical differences do not have to be considered. There may be language differences, but since English is the language most used in international universities, we expect the users to understand English on a basic level. The goal of the students using our code coach is to become better programmers (generally speaking). The overall goal is to teach beginner students a good programming practice that prevents them from becoming frustrated during coding which could potentially make them give up their studies. CodeCoach is designed to be a personal coach that does not interact with the teacher, but only with students during their individual coding sessions.
%\item \textbf{Activities} The overall purpose of the activity is to improve programming practices. This is a vague-defined iterative activity that can take arbitrary amounts of time. The start of this activity is when the user first uses the CodeCoach to check their code. The progress from this point can be measured approximately by the feedback of our CodeCoach. CodeCoach is agnostic of time and frequency when it is used, i.e. the CodeCoach activity can be paused for an arbitrary time and then be resumed without influencing its functionalities. It does not require cooperation with others (but a cooperation with fellow students or teachers could improve the overall development process). The activity is not safety-critical, since CodeCoach is run locally on personal computers and does not communicate with a central repository or similar.
%\item \textbf{Contexts} Physically the activity takes place presumably indoors in computer pools or at student homes. Socially it may happen in student groups, but the user will most likely do their programming exercises on their own, since the goal in first programming courses is to develop individual programming skills (not techniques for pair-programming for example). Organizationally, the context is private or educational, depending on the use decided by the programming teacher.
%\item \textbf{Technologies} The input to the system is code in a specific programming language. This will be entered via a keyboard. Operations like saving, importing or debugging code might also occur when using CodeCoach integrated into an IDE. The output of the system is textual and visual feedback on the quality of the code which is displayed via the CLI or IDE. Communication with a local database is needed to allow tracking the code development.
%\end{itemize}
%Concrete requirements:
%\begin{itemize}
%\item The UI will function either in the form of pop-ups that will appear at the end of every line, notifying the programmer about the potential nasty practices employed, or similar to the MS Office PaperClip.
%\item UI will show information about the offending lines, the nature of the bad practice, and links to resources describing how this practice can be remedied. The suggested remedy can be generic,
%\item not annoy
%\item be flexible, adaptable
%\item fit the IDE's design, mental model
%\end{itemize}

%Requirements
%-Platform independent
%-Have a BPR 
%-Easily installable (less than x clicks, only 1 package, not git separately e.g.)
%-Stakeholder: Claudia, Artur, Programmers, Students
%-What are their requirements
%-Language independent (english git / german git)

\subsection{Software Interfaces} \label{ss:softwarereq}
\paragraph{IDE Integration Requirements}
To support beginner programmers in particular, CoCo should not only be run on the command line, but also be integrated into at least one IDE.
To choose an IDE that CoCo should extend by a plugin, we identified the following requirements:
(1) The IDE should be convenient for beginner programmers or even give special support to beginner programmers. (2) The IDE should be easily extensible by custom plugins. (3) The IDE should be a \enquote{common} IDE that is often used (to avoid beginner programmers and teachers needing to migrate to another IDE).\\
We found PyCharm as the IDE that fulfills these requirements best. PyCharm is open source and easily extensible. It offers a IntelliJ plugin interface, which enables to implement plugins in Java using the JDK 1.7.
Additionally to the Community Edition, PyCharm is available as \enquote{PyCharm Edu}, which is a PyCharm edition especially designed to beginner programmers, offering a simpler interface and beginner tutorials.
%Our plugin uses CoCo to integrate its functionallity into PyCharm. The input interface to CoCo is the commandline-parameter which takes the current file as input.
%The communication interface from CoCo to the plugin is stdout. The plugin reads CoCo's output from stdout.
%TODO specify communication protocol?
%A data structure for this communication is defined in: .... 
%The plugin itself uses the IntelliJ plugin interface and is developed with jdk-1.7.

\paragraph{Source Code Checking Requirements}

There are already libraries available for Python source code checking, especially for checking against conventions such as the PEP-8 style guide \cite{coghlan_pep_????}.
We thus decided to build on an existing Python source code checker and extend it for our needs.
For a Python source code checker to build on, we identified four requirements: (1) It should already cover basic style guides such as the PEP-8 style guide. (2) It should be easily extensible, preferably using Python. (3) It should present its output in a machine readable format, to be easily parsable by CoCo. (4) It should run on any platform that supports Python.\\
%As a convenient solution matching these requirements, we %identified Pylint \cite{_pylint_????}.
%Pylint is written in Python and extensible by writing customized checking modules called \emph{Checker}.
%We introduced a new error category in Pylint named \emph{Beginner Errors} and set up several checkers to find mistakes commonly made by beginners. A detailed description of the checkers implemented can be found in section \ref{ss:bpr}.

\paragraph{Database Requirements}

As described in Section \ref{ss:et}, CoCo's Error Tracking component requires a database to keep track of code diffs and error statistics.
The database should be available without any user interaction, especially the user should not be required to install a separate database management system.
%This is the reason why server based database management systems (as MySQL, PostgreSQL) don't qualify for our task.
%Instead we opt in for SQLite, a database management system which is already integrated in our choosen runtime environment Python 2.7.
%SQLite doesn't require any separate database server processes and is thus a lightweight solution for our requirements.
%It enables to query and update the Error Tracking database using SQL queries, a fact that simplifies the aggregation of error information and calculation of error statistics.

\section{Software System Attributes}

\begin{table}[!ht]
\centering
\caption{List of software system attributes (non-functional requirements).}
\resizebox{0.5\columnwidth}{!}{
\begin{tabular}{l|l}
Name & Description \\
\midrule
Portability & CoCo should be usable on any platform suitable for Python programming. \\
Maintainability & CoCo should easily be extensible e.g. by new Pylint checkers. \\
Performance & CoCo should make programming easier. It should thus integrate well into \\
& the programming workflow, results should be displayed immediately.\\
Security & CoCo doesn't have any strict security requirements, however user specific \\
& data (as the error tracking statistics) should not be accessible to other \\
& users without permission.\\
Emotional factors & CoCo should motivate to learn programming. \\
%&Our approach to this goal is the implementation of gamification features.\\
Usability & CoCo should be usable by beginners without special training.
\end{tabular}
}
\label{tab:nfreq}
\end{table}

Software System Attributes are non-functional requirements that CoCo should provide.
This includes requirements that cover quality characteristics of CoCo in contrast to specific functions of the software.
Table \ref{tab:nfreq} encloses a list of software system attributes that we identified to be important for consideration for the implementation of CoCo.

%\chapter{Additional Material}
