#!/usr/bin/env python2.7
# -*- coding: utf-8 -*-

# Internal modules
import os
import sys
import hashlib
import shutil
import sqlite3
import inspect

# External modules
import sh
cocopath = os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))
sys.path[0:0] = [cocopath + '/astroid/']
sys.path[0:0] = [cocopath + '/logilab/']
sys.path[0:0] = [cocopath + '/pylint4coco/']
from pylint4coco.pylint.lint import Run
from pylint4coco.pylint.reporters.text import TextReporter


def main():
    print('Coco Prototype')

    # 1. Get file name and absolute path to run
    if len(sys.argv) <= 1:
        print('Usage: coco.py <Python file>')
        return

    absfile = os.path.abspath(sys.argv[1])
    filebasename = os.path.basename(absfile)
    abspath = os.path.dirname(absfile)
    abspathhash = hashlib.md5(abspath).hexdigest()

    # 2a. Initialize local Git repository
    gitrootdir = os.path.expanduser('~/.coco/ErrorTrackingGits/%s' % abspathhash)
    git = sh.git.bake(_cwd=gitrootdir)
    newgit = False
    if not os.path.exists(gitrootdir):
        print('Creating new Git repository in %s' % gitrootdir)
        os.makedirs(gitrootdir)
        git.init()
        newgit = True

    # 2b. Initialize SQLite
    sqliteconnection = sqlite3.connect(os.path.expanduser('~/.coco/tracking.db'))
    sqlitecursor = sqliteconnection.cursor()
    sqlitecursor.execute('''CREATE TABLE IF NOT EXISTS tracking(
        id INTEGER PRIMARY KEY ASC,
        project TEXT,
        error_source TEXT,
        error_type TEXT,
        error_number TEXT,
        error_message TEXT,
        date_occured TEXT,
        date_fixed TEXT,
        diff TEXT
        );''')
    sqliteconnection.commit()

    # 3. Copy file and add to Git (commit later)
    shutil.copy2(absfile, gitrootdir)
    git.add(filebasename)

    # 4. Get errors from the last run
    lasterrorids = []
    if not newgit:
        for line in git.reflog(_tty_out=False):
            if 'Coco auto commit' in line:
                for errorid in line.partition('Coco auto commit ')[2].split(', '):
                    lasterrorids.append(errorid.strip())
                break

    # 5. Check pylint for errors
    class WritableObject:
        def __init__(self):
            self.content = []

        def write(self, string):
            self.content.append(string)
    pylint_output = WritableObject()

    Run([
        sys.argv[1],
        '--msg-template="PYLINTMESSAGE:::{line}:::{column}:::{msg_id}:::{msg}"',
        '--reports=n',
        '--persistent=n'
        ],
        reporter=TextReporter(pylint_output),
        exit=False)

    errors = []
    has_compilererror = False
    print('Errors found:')
    for line in pylint_output.content:
        if line.startswith('PYLINTMESSAGE'):
            trash, line, column, msg_id, msg = line.split(':::')
            errors.append({
                'line': line,
                'column': column,
                'msg_id': msg_id,
                'msg': msg,
            })
            if msg_id.startswith('E'):
                has_compilererror = True
            print('  %s:%s %s %s' % (line, column, msg_id, msg))

            # Check in tracking database if such an error occured previously
            # TODO Only do this for errors which also have the same error message, not just err id
            for i, row in enumerate(sqlitecursor.execute('SELECT diff FROM tracking WHERE error_number=?', (msg_id,))):
                if not row or not row[0]:
                    continue
                if not i:
                    print('    This is how you fixed this error the last times:')
                print('    SOLUTION #%i --------------------------------' % i)
                printremainer = False
                for line in row[0].split('\n'):
                    if line.startswith('@@'):
                        printremainer = True
                        continue
                    if printremainer:
                        if 'No newline at end of file' not in line:
                            print('      %s' % line)
            print('  ~~~~~~~~~~~~~~~~~~~~')
    if not errors:
        print('  Kewl! No errors!')

    # 6. Commit Git repository and add errors found
    # TODO This will fail in localized git versions! (e.g. for German messages)
    if 'nothing to commit' not in git.status():
        # TODO Git has to be set up in order to do this (user.email and user.name have to be set)
        git.commit(m='Coco auto commit %s' % (', '.join(error['msg_id'] for error in errors) if errors else 'NO ERRORS'))

    # 7. Compute which errors have been fixed by this run
    currenterrorids = [error['msg_id'] for error in errors]
    fixederrorids = []
    print('Errors fixed:')
    for lasterrorid in lasterrorids:
        # Don't count anything but Exxx and Bxxx errors when a compiler error
        # occured (since they prevent other errors from being displayed)
        if has_compilererror and lasterrorid[0] not in ['B', 'E']:
           print('  The compiler error prevents a %s-error from being displayed.' % lasterrorid)
           continue
        if lasterrorid not in currenterrorids:
            fixederrorids.append(lasterrorid)
            print('  %s' % lasterrorid)

    # 8. Get diff if errors have been fixed and insert them into SQLite
    if not fixederrorids:
        print('  No errors fixed since the last check.')
    else:
        diff = git.diff('HEAD~1', _tty_out=False)
        errordata = [(
                         abspath,
                         'pylint',
                         fixederrorid,
                         str(diff)
                     ) for fixederrorid in fixederrorids]
        sqlitecursor.executemany('''INSERT INTO tracking (
            project,
            error_source,
            error_number,
            date_fixed,
            diff
            ) VALUES (?, ?, ?, datetime(), ?)''', errordata)
        sqliteconnection.commit()

    sqliteconnection.commit()
    sqliteconnection.close()

if __name__ == '__main__':
    main()
