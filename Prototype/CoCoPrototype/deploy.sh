#!/bin/bash
find . -name \*.pyc -delete
tar -zcvf CocoPrototype.tar.gz astroid/* coco.py examples/ pylint4coco/* pylintrc reset.sh sh.py
