from pylint.interfaces import IRawChecker
from pylint.checkers import BaseChecker
import re


class BeginnerChecker(BaseChecker):
    """check for beginner errors
    """

    __implements__ = IRawChecker

    name = 'beginner'
    """
    msgs = {'B0001': ('%s space %s %s %s\n%s',
              'bad-whitespace',) ('Used when a wrong number of spaces is used around an operator, '
               'bracket or block opener.'))
    }
    then later: args=(count, state, position, construct,
                                   _underline_token(token)))
    """
    msgs = {'B0001': ('Use "==" for comparison\n%s%s\n', "'==' instead of '=' for comparison", "Use '==' instead of '=' for comparison of variable values, use '=' for assigning values"),
			'B0002': ('First the greater or less than sign, then the equal sign',
                      'Switch = and < or >',
                      ('As spoken: Greater than or equal >=',
                       'Less than or equal <=')),            
		}
    options = ()
    priority = -1


    def process_module(self, module):
        """process a module
        the module's content is accessible via node.stream() function
        """
        try:
            with module.stream() as stream: #module is ast
                for (lineno, line) in enumerate(stream):
                    pattern = "\s*if[^=<>!]*=[^=<>!]+"
                    matcher = re.compile(pattern)
                    if matcher.findall(line):
                    #if "if" in line and "=" in line and not "==" in line:
                    # and also no other operator involving "=" is meant like <=,>=,!=
                        self.add_message("B0001", line=lineno)

                    # Switching less/greater than or equal
                    pattern2 = "\s*if[^<>!]*=[<>!]"
                    matcher2 = re.compile(pattern2)
                    if matcher2.match(line):
                    #if "if" in line and there is an =</=> instead of <=/>=
                        self.add_message("B0002", line=lineno)

        except AttributeError: #module is not ast, but filepath
            with open(module, "r") as stream:
                for (lineno, line) in enumerate(stream):
                    pattern = "\s*if[^=<>!]*=[^=<>!]+"
                    matcher = re.compile(pattern)
                    p = matcher.findall(line)
                    for pi in p:
                        a = pi.find("=")
                        b = pi.find("==")
                        if a!=b:
                            i = a
                        else:
                            i = pi.find("=", b+2)
                        self.add_message("B0001", line=lineno, args=(pi, "\n"+(i)*" "+"^"))

				 	# Switching less/greater than or equal
                    pattern2 = "\s*if[^<>!]*=[<>!]"
                    matcher2 = re.compile(pattern2)
                    if matcher2.match(line):
                    #if "if" in line and there is an =</=> instead of <=/>=
                        self.add_message("B0002", line=lineno)


def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(BeginnerChecker(linter))
