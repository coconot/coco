# README #

### CoCo Code Coach for Teaching Best Practices ###

* Designed for novice programmers, to assist them in learning to write clean code 
* Supports Python
* Builds on Pylint
* Error tracking allows to inspect, organize and learn from own errors
* CLI and PyCharm IDE Plugin

### CLI Prototype Version 0.1 ###

At the current stage, only the CLI prototype is available for installation. Find out more by following the instructions in Prototype/CoCoPrototype/README.txt.

### CoCo Team ###

Contact us!

* Dominik Sterk (d.sterk@stud.uni-heidelberg.de) 
* Sophia Stahl (sophia.stahl@stud.uni-heidelberg.de) 
* Jan Pawellek (pawellek@stud.uni-heidelberg.de)
* Julia Kreutzer (j.kreutzer@stud.uni-heidelberg.de)