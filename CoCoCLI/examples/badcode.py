"""
This is an example for bad code with typical beginner errors
Use this code to improve it with CoCo :)
"""
import random

def orderpizza(topping="nothing", size=30):
    """Ordering a pizza"""
    print "\nOrdering your pizza with %s topping, d=%dcm" % (topping, size)
    if topping = "tuna": #B001
        print "Sorry, tuna's out."
        topping = "no tuna"
    if size => 50: #B002
        print "Sorry, that's not good for your health"
        topping = "veggie"
        size = "veggie"
        size = 5
    print "Your pizza (d=%dcm) with topping %s was ordered" % (size, topping)
    print "Delivery in %d minutes" % (random.randint(0, 100))

if __name__ == "__main__":

    orderpizza("cheese", 40)
    orderpizza("bolognese", 70)
    orderpizza("tuna")

