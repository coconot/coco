---------------------------------------------------------
---------------------------------------------------------
            Code Coach (CoCo) CLI Prototype                          
---------------------------------------------------------
---------------------------------------------------------
 Version: 0.1                                          
 Authors:                                              
    Jan Pawellek (pawellek@stud.uni-heidelberg.de)     
    Sophia Stahl (sophia.stahl@stud.uni-heidelberg.de) 
    Julia Kreutzer (j.kreutzer@stud.uni-heidelberg.de) 
 Date: August 12, 2015                                 
---------------------------------------------------------
---------------------------------------------------------


---------------------------------------------------------
1. Prerequisites
---------------------------------------------------------
The following software is required:
- python2.7
- git
Make sure that python2.7 is on your pythonpath.
For CoCo in the current stage, it is required that you run it in English. 
On a Linux machine, setting the git language can for example be done with the following prompt:
>> echo "export LANGUAGE='en_US.UTF-8 git'" >> ~/.bashrc

---------------------------------------------------------
2. Setup
---------------------------------------------------------
Extract the tar.gz archive.
Move to the extracted directory.

---------------------------------------------------------
3. Usage
---------------------------------------------------------
Run CoCo on your code with the following command:
>> python coco.py <your_code>.py

For a first try, you might follow the example in Section 4.

The output of CoCo will provide you with:
1) a list of errors that were detected by our extended pylint (position in file, error message)
2) a list of errors you have fixed since the last run of CoCo
3) a list of pevious fixes for errors of the same category as the detected ones

What can you do with CoCo?
1) Detect best practice, typical beginner, compiler, and pylint errors in your code before actually running it.
2) Be encouraged when having sucessfully fixed an error.
3) Learn from your own past errors and fixing strategies and take previous solutions as recommendations.
4) In general: Track the development of your code and improve your coding skills.

If you want to reset the error tracking completely, run the reset.sh script. 

---------------------------------------------------------
4. Example
---------------------------------------------------------
The directory examples contains a python code example called "badcode.py".
As the name suggests, this code contains some errors that we will detect, analyse, and correct with CoCo's help in a small walk-through example.

1) Run 
    >> python coco.py examples/badcode.py
2) Inspect CoCo's output. 
    CoCo detects three errors: 
    A syntax error (E0001) and a beginner error (B0001) in l.10, and another beginnner error (B0002) in l.13.
    The messages following the error ids should give you some hint how to correct these errors.
3) Correct B0001 in l.10 with the help of CoCo's B0001 error message.
4) Then run CoCo again on the code (see 1)).
5) Yay, you fixed the error B0001! Two more to go.
6) Correct B0002 in l.13.
7) Well done, no errors left! 
    You might realize that the last correction actually fixed two errors at a time, since the syntax error originated from the very same place as the beginner error.
8) Let's introduce a new error. Change "==" to "=" in l.21 (--> 'if __main__=__name__:' ).
9) Re-run CoCo on the code.
10) This time the CoCo output contains more than just a list of errors. Additionally, it shows you previous fixes of similar errors.
    In our case, this is helpful for fixing the B0001 error, we just introduced in 8).
    Most important, the "+" and "-" signs indicate where changes have been applied to the code.
11) Fix the error introduced in 8) with the help of the information how you fixed it last time.
12) Well done, the code is repaired. And you have completed the walk-through example. 
    Now you should know CoCo's basic functionalities. It's time to run it on your own code!


---------------------------------------------------------
5. Further Remarks
---------------------------------------------------------
- CoCo's error detection builds on pylint. You can find more information (e.g. on error types) here: http://www.pylint.org/
- CoCo's error tracking uses git for creating diffs of your code. For getting used to its notation, you might be interested in this website: http://git-scm.com/docs/git-diff
- CoCo stores your errors in a sqlite database in the hidden directory .coco in your home directory. In case you want to inspect the database, e.g. with an sqlite browser, open the ~/.coco/tracking.db file.
- Note the differences between running pylint and CoCo on your bad code and directly running it with python.
- The collection of novice errors we can detect so far are rather trivial, that's just our starting point.
- Until now, the error "recommendation" is only in its rudimentary stages. You might have realized that if you fix more than two errors at a time and later on meet one of these errors, CoCo points you to the solution of both errors although one of them is irrelevant.
- We haven't worked out how to properly treat the case when "one" error in the code produces several errors in CoCo -  this will follow soon.
- The output of CoCo is not the nicest so far, some color will be added especially for the diff messages in the error tracking part.
- When a syntax error occurs, no further syntax errors can be found, since the code cannot further be parsed. This is a characteristic of pylint that we have to deal with.
- We'd love to improve CoCo, so send us your feedback! :)
