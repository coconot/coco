\documentclass[journal]{vgtc}                % final (journal style)
%\documentclass[review,journal]{vgtc}         % review (journal style)
%\documentclass[widereview]{vgtc}             % wide-spaced review
%\documentclass[preprint,journal]{vgtc}       % preprint (journal style)
%\documentclass[electronic,journal]{vgtc}     % electronic version, journal

%% Uncomment one of the lines above depending on where your paper is
%% in the conference process. ``review'' and ``widereview'' are for review
%% submission, ``preprint'' is for pre-publication, and the final version
%% doesn't use a specific qualifier. Further, ``electronic'' includes
%% hyperreferences for more convenient online viewing.

%% Please use one of the ``review'' options in combination with the
%% assigned online id (see below) ONLY if your paper uses a double blind
%% review process. Some conferences, like IEEE Vis and InfoVis, have NOT
%% in the past.

%% Please note that the use of figures other than the optional teaser is not permitted on the first page
%% of the journal version.  Figures should begin on the second page and be
%% in CMYK or Grey scale format, otherwise, colour shifting may occur
%% during the printing process.  Papers submitted with figures other than the optional teaser on the
%% first page will be refused.

%% These three lines bring in essential packages: ``mathptmx'' for Type 1
%% typefaces, ``graphicx'' for inclusion of EPS figures. and ``times''
%% for proper handling of the times font family.

\usepackage{mathptmx}
\usepackage{graphicx}
\usepackage{times}
%\usepackage{epstopdf}


%% We encourage the use of mathptmx for consistent usage of times font
%% throughout the proceedings. However, if you encounter conflicts
%% with other math-related packages, you may want to disable it.

%% This turns references into clickable hyperlinks.
\usepackage[bookmarks,backref=true,linkcolor=black]{hyperref} %,colorlinks
\hypersetup{
  pdfauthor = {},
  pdftitle = {},
  pdfsubject = {},
  pdfkeywords = {},
  colorlinks=true,
  linkcolor= black,
  citecolor= black,
  pageanchor=true,
  urlcolor = black,
  plainpages = false,
  linktocpage
}

%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% allow for this line if you want the electronic option to work properly
\vgtcinsertpkg

%% In preprint mode you may define your own headline.
%\preprinttext{To appear in IEEE Transactions on Visualization and Computer Graphics.}

%% Paper title.

\title{SCORE Project: CodeCoach - Coaching Best Coding Practices}

%% This is how authors are specified in the journal style

%% indicate IEEE Member or Student Member in form indicated below
\author{Julia Kreutzer, Jan Pawellek, Sophia Stahl \\ Heidelberg University, May 12, 2015}
\authorfooter{
%% insert punctuation at end of each item
Contact:  J.Kreutzer@stud.uni-heidelberg.de, Pawellek@stud.uni-heidelberg.de, sophia.stahl@stud.uni-heidelberg.de
}


%other entries to be set up for journal
%\shortauthortitle{Biv \MakeLowercase{\textit{et al.}}: Visualizing RNA Secondary Structure with Base Pair Binding Probabilities}
%\shortauthortitle{Firstauthor \MakeLowercase{\textit{et al.}}: Paper Title}

% Abstract section.
%\abstract{Here we describe our first advances on our way to teach beginner programmers about coding best practices while they are learning. } 
% end of abstract


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

%% The ``\maketitle'' command must be the first command after the
%% ``\begin{document}'' command. It prepares and prints the title block.

%% the only exception to this rule is the \firstsection command
\firstsection{Goal and Motivation}
%Julia
\maketitle
%% \section{Introduction} %for journal use above \firstsection{..} instead


This project is oriented towards a participation in the SCORE contest 2016. In particular, we will implement a solution for the ``CodeCoach'' project task supervised by Dr. Claudia Szabo \cite{szabo_score_????}. The goal of this project is to implement a CodeCoach that supports novice programmers to learn best practices. The CodeCoach is not only designed to generally improve the students' programming skills, but furthermore to motivate the students during their early programming experiences, and to help them integrate best practices into their every-day coding right from the beginning. The CodeCoach is not a teacher in a tutorial-style setting, but an assistant that offers advice and helpful hints throughout the students' coding sessions.

Existing IDEs already help the user to debug and refactor code and follow certain style guidelines. With the CodeCoach we want to go a step further and teach the user how to write clean code to avoid possible frustration due to extensive debugging and error identification efforts. Teaching the best practices will focus on the readability of the produced code and the understanding of the motivation for those best practices. 

The following sections describe our first ideas and plans for the implementation and realization of the CodeCoach project.

%References: [1] http://score-contest.org/2016/projects/codecoach.php




\section{Approach}

The CodeCoach is a virtual coach that interacts with the learner during programming sessions, going far beyond the error reporting of the compiler. It rather offers encouraging and constructive suggestions for code improvements. Thereby, a reasoning for the suggestion should be provided to make the student understand the problem instead of blindly following advice. The violation of a best practice is explained, ideally using an example. 

As specified in the SCORE project description, the CodeCoach operates on the basis of a best practice repository (BPR) and an integrated graphical user interface (GUI).

\subsection{Best Practice Repository}
The BPR is a collection of code- and behavior-related best practices that can be considered a combination of the following categories:
\begin{itemize}
\item \textbf{programming practices and coding standards:} Guidelines regarding naming, file organization, statements and declarations, and layout and comments \cite{jalote_concise_2008}
\item \textbf{code smells and clean code heuristics:} As suggested in \cite{martin_clean_2009}
\item \textbf{complexity metrics:} Including e.g. McCabe complexity \cite{mccabe_complexity_1976} 
\item \textbf{style guidelines (language-specific):} For example pep8 for Python \cite{coghlan_pep_????} 
\item \textbf{personal software process (PSP):} As explained in \cite{ludewig_software_2013}, the concept is to support students tracking their personal software process, not in the original format which was devised for a specific system by Watts Humphrey \cite{humphrey_discipline_1995}, but to teach students to become aware of time and effort spent on different phases of development (first sketch, debugging, code reading, compiling, testing, ...). The CodeCoach gives initial suggestions on how to define phases and gives reasons why these are relevant, however also allows the users to define their own phases. Additionally, the threshold for tracking personal errors is lowered by providing an intuitive mechanism
\end{itemize}

%Some aspects are applicable to multiple programming languages while others are language-dependent. Even though we are starting from Python, we aim to make the CodeCoach usable for any language by structuring the BPR in an easily expandable manner.

This project will include a discussion on which best practices to integrate into the CodeCoach, how and why, since not all of them are directly applicable or useful for programming beginners. For example the beginners are not yet faced with the challenges that a professional developers has to deal with, such as developing a product or working in a large team with a manager. Our target group has rather smaller projects and efficiency is not the prior goal, but readability \cite{jalote_concise_2008}. 

To test the functionalities of the BPR and to use it across platforms or when students are not learning within an integrated development environment (IDE) for simplified focus on the code, a standalone command line interface (CLI) is implemented.


\subsection{Graphical User Interface}

In addition to the CLI, a GUI is integrated into an existing IDE. The CodeCoach follows human-computer interaction principles, such as consistency and shortcuts for frequent users. \cite{shneiderman_designing_1987}
The GUI enables the students to track their behavior and measure some actions or behaviors automatically, such as the number of code modifications between compilations. Another idea is to offer visualization (diagrams of errors per lines of code etc.) to make the progress visible to the user. 

%\begin{figure}[h]
%\centering
%\includegraphics[width = 0.2\textwidth, height=.15\textheight]{BadgeCodeCadamy}
%\label{fig:1}
%\caption{\textbf{Badges by CodeCadamy.com} Example for gamified learning. \cite{CodeCadamy} }
%%http://www.codecademy.com/learn
%\end{figure} 

In the e-Learning community the use of gamification is known to improve the learning outcome by adding some fun \cite{laskaris_30_2014}. One of many examples are the badges by CodeCadamy the students earns while going through Python tutorials \cite{codecadamy_codecadamy_????}. % as can be seen in fig\,\ref{fig:1}.
We also plan to use gamification techniques such as a coach avatar or mascot to come closer to a personalized coach-student interaction. 


%However, the extent of these gamified elements greatly depends on the programming environment and if individual learners are trackable. 


The challenge for our CodeCoach is to assist, but not to annoy. 
The ``anti-annoy'' strategy is to adapt to the level of programming experience the learner has. It is highly configurable by giving the learner options to choose which type of hints he wants to make use of.


\section{Implementation}
%Jan
As it is widely used as a beginner's first programming language, we implement the CodeCoach to support Python for the start, with the option to extend to C/C++ or other programming languages which are often taught at computer science courses.

Python is used as the programming language in which our CodeCoach project itself is implemented. This allows the CodeCoach analysis to be applied to its own code. Furthermore, students can take a look under the hood of CodeCoach to see how (and if) best practices are implemented.

As mentioned above, there are two main parts on an architectural view. The CodeCoach implementation consists of a command line interface (CLI) and one and possibly more user interfaces (UI). The CLI can be thought of like a command line compiler (think of gcc, for example) which is given any source file as an argument. Instead of command line errors per line, the CodeCoach command line interface will return best practice hints per line. Thus, CodeCoach's functionality can be completely accessed from the command line by people who prefer to use only this option.

On the other hand, the CLI (or an appropriate API, namely Python module interfaces) serves as a basis for implementing user interfaces which are directly integrated in common Python IDEs. For our project, we first focus on the Ninja-IDE \cite{rostagno_ninja-ide_2013}  which is a light-weight Python IDE written in Python under the GPL v3 license. The Ninja-IDE provides an extensible open architecture, thus we aim to integrate the CodeCoach UI as a plugin in the Ninja-IDE. As mentioned, the CodeCoach UI uses the CodeCoach CLI or API as a backend.

The core of the backend, or CodeCoach CLI/API, is the ability to analyse code. As our objective is to support as many different types of best practices as possible, including measures derived from code complexity metrics and code smells, we plan to implement several perspectives on code. Some metrics can easily be applied on plain text (for example counting the lines of code, applying regular expressions, etc.), others may work on advanced constructs like abstract syntax trees (ASTs).

The Python project called Flake8 \cite{cordasco_flake8_????} provides a ``modular source code checker'' under the open-source MIT license. Flake8 serves as a wrapper around source code checking tools and plugins. Thus, Flake8 provides an environment which includes a CLI and a Python API to perform source code checking. Flake8 furthermore offers several views on the source code, including a per-line view and abstract syntax trees.

Currently, Flake8 integrates a plugin checking for conformance against the Python style guidelines defined in the PEP-8 standard \cite{coghlan_pep_????}, a plugin to compute the McCabe complexity, a plugin to check for naming conventions, plugins to check for remaining ``TODO'' comments or remaining print statements for debugging, and others. We thus perceive Flake8 as a suitable basis for further investigations and maybe even as a basis for the CodeCoach development.

%[1] http://ninja-ide.org/
%[2] https://pypi.python.org/pypi/flake8
%[3] https://pypi.python.org/pypi/pep8

\section{Organization}
% Julia

Since we work on this project as a team, we make use of several collaboration tools:

    The code for the CodeCoach is stored in a bitbucket repository \cite{atlassian_bitbucket_????}.
    The development process of the project is tracked in an OpenProject backlog \cite{tesch_openproject_2015}.
    For the collection of the results of our research concerning best practices we set up a google document sheet  \cite{stahl_sophia_spreadsheet_????}.
    A Zotero group allows us to keep track of the literature that we consider relevant for the design or our CodeCoach \cite{stillman_zotero_????}.

The basis of our teamwork are weekly meetings. We decided to follow an agile development process similar to scrum, but adapted to the relatively small size of our team. At all steps of the process we will include code testing and peer reviews. We will start with a collection of requirements and an initial design of the architecture. Since we follow agile development principles, these initial findings might get modified over time, but will constitute the first basis for our implementation. The implementation as such will include several components: The implementation of the Best Practice Repository on a CLI basis, the design of the coach avatar and the implementation of the IDE plugin. Once our implementation is complete, an evaluation in real-life, e.g. with students in beginner programming courses, will follow and will help us to further improve the user interaction of our coach with the student.

%References: [1] https://bitbucket.org/coconot/coco [2] GOOGLEDOC? [3] Zotero? [4] OpenProject




%\acknowledgments{
%We gratefully acknowledge our supervising professor Prof. Dr. Artur Andrzejak.}

\bibliographystyle{IEEEtran}
%%use following if all content of bibtex file should be shown
%\nocite{*}
\bibliography{./Coco_Lib/Coco_Lib}



\end{document}




%%%%%%%%% Extra Info, maybe not needed here, but at some other time

%cite Jalote
%`" Writing solid code is a skill that can only be acuired by practice. However, based on experience, some general rules and guidelines can be given for the programmer'"
%
%Overall goal: readability
%Objective of structured programming: static and dynamic structure of the program are the same
%Achieved by single-entry, single-exit statements/control constructs, most commonly:
%Selection
%Iteration
%Sequencing

