from pylint.interfaces import IRawChecker
from pylint.checkers import BaseChecker
import re


class BeginnerChecker(BaseChecker):
    """check for beginner errors
    """

    __implements__ = IRawChecker

    name = 'beginner'
    """
    msgs = {'B0001': ('%s space %s %s %s\n%s',
              'bad-whitespace',) ('Used when a wrong number of spaces is used around an operator, '
               'bracket or block opener.'))
    }
    then later: args=(count, state, position, construct,
                                   _underline_token(token)))
    """
    msgs = {'B0001': ('Use "==" for comparison', "'==' instead of '=' for comparison", "Use '==' instead of '=' for comparison of variable values, use '=' for assigning values"),
			'B0002': ('First the greater or less than sign, then the equal sign',
                      'Switch = and < or >',
                      ('As spoken: Greater than or equal >=',
                       'Less than or equal <=')),  
            'B0003': ('Use "()" for conditions instead of "{}"', "'()' instead of '{}' for conditions", "Use '()' instead of '{}' for conditions (or leave out the brackets), use '{}' for dictionaries (e.g. d = {2:'two'}")       
		}
    options = ()
    priority = -1

    def detectB0001(self,line):
        """ Detect incorrect comparison operator = """
        pattern = "\s*(el)?if[^=<>!]*=[^=<>!]+"
        matcher = re.compile(pattern)
        if matcher.findall(line):
            return True
        else:
            return False

    def detectB0002(self,line):
        """ Switching less/greater than or equal """
        pattern2 = "\s*(el)?if[^<>!]*=[<>!]"
        matcher2 = re.compile(pattern2)
        if matcher2.match(line):
            #if "if" in line and there is an =</=> instead of <=/>=
            return True
        else:
            return False

    def detectB0003(self,line):
        """condition with {} -> {} has to be the outer most bracket"""
        # split at { and }
        if "if" in line: #trivial condition detection
            if "{" in line or "}" in line:
                prefix = line.strip().split("{")[0]
                suffix = line.strip().split("}")[1]
                #after } there should be only the ":"
                if len(suffix.replace(":","").strip()) < 1:
                    #before { there should be only the "if"
                    if len(prefix.replace("if","").strip()) < 1 or len(prefix.replace("elif", "").strip()) < 1:
                        #if there is only a dictionary declaration, do nothing (e.g. if {2:3}: is a valid expression)
                        if not ":" in line.strip().split("{")[1].split("}")[0]:
                            return True
        else:
            return False
    
    def detectBs(self,line,lineno):
        if self.detectB0001(line):    
            self.add_message("B0001", line=lineno)
        if self.detectB0002(line):
            self.add_message("B0002", line=lineno)
        if self.detectB0003(line):
            self.add_message("B0003", line=lineno)


    def process_module(self, module):
        """process a module
        the module's content is accessible via node.stream() function
        """
        try:
            with module.stream() as stream: #module is ast
                for (lineno, line) in enumerate(stream):
                    self.detectBs(line, lineno)

        except AttributeError: #module is not ast, but filepath, in case of syntax error
            with open(module, "r") as stream:
                for (lineno, line) in enumerate(stream):
                    self.detectBs(line, lineno)

def register(linter):
    """required method to auto register this checker"""
    linter.register_checker(BeginnerChecker(linter))
