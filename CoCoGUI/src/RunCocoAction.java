import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.execution.filters.TextConsoleBuilderFactory;
import com.intellij.execution.ui.ConsoleView;
import com.intellij.execution.ui.ConsoleViewContentType;
import com.intellij.openapi.actionSystem.CommonDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.application.ApplicationManager;
import com.intellij.openapi.editor.Document;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.fileEditor.FileDocumentManager;
import com.intellij.openapi.fileEditor.FileEditorManager;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.ui.Messages;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.openapi.wm.ToolWindow;
import com.intellij.openapi.wm.ToolWindowAnchor;
import com.intellij.openapi.wm.ToolWindowManager;
import com.intellij.ui.content.Content;
import com.intellij.openapi.util.IconLoader;

/*Jajaja coco jambo :-D Imports to play music*/

import javax.swing.*;
import java.io.FileNotFoundException;
import java.util.Scanner;



import static java.lang.Thread.sleep;

/**
 * Created by dominik on 27.10.15.
 */
public class RunCocoAction extends AnAction{

    //protected static ConsoleView cocoConsole = null;
    private ICoco ic = new CocoAccess();
    //private static CocoCheerUpWindow ccuw = new CocoCheerUpWindow();
    private static CocoUIWindow cuiw = new CocoUIWindow();


    public void actionPerformed(AnActionEvent e) {
        final Project project = (Project) e.getData(PlatformDataKeys.PROJECT);
        final ToolWindowManager manager = ToolWindowManager.getInstance(project);
        final Editor editor = (Editor) e.getData(CommonDataKeys.EDITOR);
        final String CocoId="Coco";


        /*Register ToolWindows*/
        //ToolWindow cocoPlain = null;
        ToolWindow cocoUI = null;
        //ToolWindow cocoCheerUp = null;

        Content content;


        /*Nur registrieren wenn nicht schon geschehen, da sonst exception */
        if(manager.getToolWindow("Coco Interface") == null){
            //cocoConsole =  TextConsoleBuilderFactory.getInstance().createBuilder(project).getConsole();
            //cocoPlain = manager.registerToolWindow("Coco Plain Output", true, ToolWindowAnchor.BOTTOM);
            //cocoCheerUp = manager.registerToolWindow("Coco Tree", true, ToolWindowAnchor.BOTTOM);
            cocoUI = manager.registerToolWindow("Coco Interface", true, ToolWindowAnchor.RIGHT);

            //Icon setzen
            //cocoPlain.setIcon(IconLoader.getIcon("coco_ico.png"));
            //cocoCheerUp.setIcon(IconLoader.getIcon("coco_ico.png"));
            cocoUI.setIcon(IconLoader.getIcon("coco_ico.png"));



            //content = cocoPlain.getContentManager().getFactory().createContent(cocoConsole.getComponent(), "", true);
            //Content content2 = cocoCheerUp.getContentManager().getFactory().createContent(ccuw, "", true);
            Content uiContent = cocoUI.getContentManager().getFactory().createContent(cuiw, "",true);
            //cocoPlain.setSplitMode(true, null);

            //cocoPlain.getContentManager().addContent(content);
            //cocoCheerUp.getContentManager().addContent(content2);
            cocoUI.getContentManager().addContent(uiContent);

            // Keine Ahnung ob null hier eine gute Idee ist aber es schreint zu funktionieren
            //cocoCheerUp.show(null);
            //cocoPlain.show(null);
            cocoUI.show(null);
        }


        //cocoConsole.clear();
        //cocoConsole.print("Saving project and starting Coco..." + "\n", ConsoleViewContentType.NORMAL_OUTPUT);


        /*Alles speichern*/
        project.save();
        ApplicationManager.getApplication().saveAll();

        /*Finde Pfad zu aktuell geöffneter Datei*/
        Document currentDoc = FileEditorManager.getInstance(project).getSelectedTextEditor().getDocument();
        VirtualFile currentFile = FileDocumentManager.getInstance().getFile(currentDoc);
        String fileName = currentFile.getPath();

        String msg = ic.runCoco(fileName);
        //cocoConsole.print(msg + "\n", ConsoleViewContentType.NORMAL_OUTPUT);
        ic.parseCocoOutput(msg, cuiw);


    }
}
