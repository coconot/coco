
import javax.swing.*;
import java.io.FileNotFoundException;
import java.net.URI;
import java.net.URL;
import java.util.Map;


/**
 * Created by dominik on 25.01.16.
 */
public class CoCoResourceManager {

    public CoCoResourceManager(){

    }

    public URL getCoCoSound() throws FileNotFoundException {
        URL s = this.getClass().getResource("cocojambo.wav");
            if(s.equals(null))
                throw new FileNotFoundException();
        System.out.println("Sound s " + s);
        return s;
    }

    //TODO: Fix path to CoCo
    public String getCoCo() throws FileNotFoundException {
        // Set the environment variable in ~/.profile e.g:
        // export COCO_CLI="/home/USERNAME/Repositories/coco/CoCoCLI/coco.py"
        // and re-login to apply the changes.
        String envpath = System.getenv().get("COCO_CLI");
        String s;
        if (envpath != null) {
            s = envpath;
        }
        else {
            s = this.getClass().getResource("CoCoCLI/coco.py").getPath();
        }

        if(s.startsWith("file:")) {
            s = s.replaceFirst("file:", "");
        }
        if(s.equals(null))
            throw new FileNotFoundException(s);
        return s;
    }

    public URL getCoCoURL() throws FileNotFoundException {
        URL s;
        s = this.getClass().getResource("CoCoCLI/coco.py");
        if(s.equals(null))
            throw new FileNotFoundException(s.toString());
        return s;
    }
    public URL getCoCoIcon() throws FileNotFoundException {
        URL s = this.getClass().getResource("coco_ico.png");
        if(s.equals(null))
            throw new FileNotFoundException();
        return s;
    }

    public URL getCoCoPalmTreeURL(int nutCount) throws FileNotFoundException {
        URL s;
        int NumberOfPalmGifs = 4;
        nutCount %= NumberOfPalmGifs;
        switch(nutCount){
            case 0:{
                s = this.getClass().getResource("palmdropcoco_static_Start.png");
                break;
            }
            case 1:{
                s = this.getClass().getResource("palmdropcoco_gifmaker1.gif");
                break;
            }
            case 2:{
                s = this.getClass().getResource("palmdropcoco_smaller_gifmaker2.gif");
                break;
            }
            case 3:{
                s = this.getClass().getResource("palmdropcoco_smaller_gifmaker3.gif");
                break;
            }
            default:
                s = this.getClass().getResource("palmdropcoco_static_Start.png");
        }

        // Produce artifacts: palem_small.gif, PalmDropCoco_smaller.gif, PalmDropCocoSmall.gif (but not as bad), PalmDropCoco.gif (and way too big)
        // Way too big: palme.gif
        // Static: palme_klein.gif
        // No artifact: palmdropcoco_gifmaker1.gif
        if(s.equals(null))
            throw new FileNotFoundException();

        return s;
    }

}
