import com.intellij.ui.AncestorListenerAdapter;
import com.intellij.ui.components.JBPanel;
import com.intellij.ui.components.JBScrollPane;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import java.awt.*;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;

/**
 * Created by dominik on 21.11.15.
 */
public class CocoUIWindow extends JBPanel {

    /*Palmtree Window stuff*/
    private JBPanel CoCoPalmtreePanel;
    private JLabel picLabel;
    private ImageIcon imageIcon = null;
    private static int replayCount = 0;
    CoCoResourceManager crm = new CoCoResourceManager();


    /*CoCo UI Stuff*/
    private JPanel CoCoUIPanel;
    private JTabbedPane tabbedPane;

    private JTextPane beginnerArea;
    private JTextPane refactorArea;
    private JTextPane conventionArea;
    private JTextPane warningArea;
    private JTextPane errorArea;
    private JTextPane fatalArea;

    private HTMLDocument beginnerDoc = new HTMLDocument();
    private HTMLDocument refactorDoc = new HTMLDocument();
    private HTMLDocument conventionDoc = new HTMLDocument();
    private HTMLDocument warningDoc = new HTMLDocument();
    private HTMLDocument errorDoc = new HTMLDocument();
    private HTMLDocument fatalDoc = new HTMLDocument();

    private HTMLEditorKit kit = new HTMLEditorKit();

    private int beginnerErrorCount = 0;
    private int refactorErrorCount = 0;
    private int conventionErrorCount = 0;
    private int warningErrorCount = 0;
    private int errorErrorCount = 0;
    private int fatalErrorCount = 0;

    private MyHyperLinkAdapter hil;

    public CocoUIWindow(){

        CoCoPalmtreePanel = new JBPanel();
        CoCoPalmtreePanel.setVisible(true);
        CoCoPalmtreePanel.setLayout(null);

        picLabel = new JLabel();
        picLabel.setSize(new Dimension(148,165));

        CoCoPalmtreePanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                picLabel.setLocation(e.getComponent().getWidth()/2-74, e.getComponent().getHeight()-165);
            }
        });
        try {
            imageIcon = new ImageIcon(crm.getCoCoPalmTreeURL(0));
            picLabel.setIcon(imageIcon);
            CoCoPalmtreePanel.add(picLabel);
        }catch(Exception x){
            JOptionPane.showMessageDialog(null, x.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }



        CoCoUIPanel = new JPanel();
        CoCoUIPanel.setMinimumSize(new Dimension(400,300));
        CoCoUIPanel.setMaximumSize(new Dimension(400,300));
        CoCoUIPanel.setLayout(null);
        CoCoUIPanel.setVisible(true);


        hil = new MyHyperLinkAdapter();
        tabbedPane = new JTabbedPane(JTabbedPane.TOP, JTabbedPane.SCROLL_TAB_LAYOUT);

        beginnerArea = new JTextPane();
        beginnerArea.setEditorKit(kit);
        beginnerArea.setDocument(beginnerDoc);
        beginnerArea.setEditable(false);
        JBScrollPane beginnerPane = new JBScrollPane( beginnerArea );
        beginnerArea.addHyperlinkListener(hil);

        refactorArea = new JTextPane();
        refactorArea.setEditorKit(kit);
        refactorArea.setDocument(refactorDoc);
        refactorArea.setEditable(false);
        JBScrollPane refactorPane = new JBScrollPane( refactorArea );
        refactorArea.addHyperlinkListener(hil);


        conventionArea = new JTextPane();
        conventionArea.setEditorKit(kit);
        conventionArea.setDocument(conventionDoc);
        conventionArea.setEditable(false);
        JBScrollPane conventionPane = new JBScrollPane( conventionArea );
        conventionArea.addHyperlinkListener(hil);

        warningArea = new JTextPane();
        warningArea.setEditorKit(kit);
        warningArea.setDocument(warningDoc);
        warningArea.setEditable(false);
        JBScrollPane warningPane = new JBScrollPane( warningArea );
        warningArea.addHyperlinkListener(hil);

        errorArea = new JTextPane();
        errorArea.setEditorKit(kit);
        errorArea.setDocument(errorDoc);
        errorArea.setEditable(false);
        JBScrollPane errorPane = new JBScrollPane( errorArea );
        errorArea.addHyperlinkListener(hil);

        fatalArea = new JTextPane();
        fatalArea.setEditorKit(kit);
        fatalArea.setDocument(fatalDoc);
        fatalArea.setEditable(false);
        JBScrollPane fatalPane = new JBScrollPane( fatalArea );
        fatalArea.addHyperlinkListener(hil);


        tabbedPane.addTab("Beginner",beginnerPane);
        tabbedPane.addTab("Refactor",refactorPane);
        tabbedPane.addTab("Convention",conventionPane);
        tabbedPane.addTab("Warning",warningPane);
        tabbedPane.addTab("Error",errorPane);
        tabbedPane.addTab("Fatal",fatalPane);

        CoCoUIPanel.addComponentListener(new ComponentAdapter() {
            @Override
            public void componentResized(ComponentEvent e) {
                super.componentResized(e);
                tabbedPane.setSize(e.getComponent().getWidth(),e.getComponent().getHeight());
            }
        });
        CoCoUIPanel.add(tabbedPane);

        this.setLayout(new GridLayout(2,1));
        this.add(CoCoUIPanel);
        this.add(CoCoPalmtreePanel);
        this.setVisible(true);
    }

    public void replayPalmTree(){

        replayCount++;
        try {
            imageIcon = new ImageIcon(crm.getCoCoPalmTreeURL(replayCount));
            picLabel.setIcon(imageIcon);
        }catch(Exception x){
            JOptionPane.showMessageDialog(null, x.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        imageIcon.getImage().flush();
    }

    private JTextPane getTextArea(String paneName) {
        switch (paneName) {
            case "B":
                return beginnerArea;
            case "R":
                return refactorArea;
            case "C":
                return conventionArea;
            case "W":
                return warningArea;
            case "E":
                return errorArea;
            case "F":
                return fatalArea;
            default:
                return null;
        }
    }

    public void clearPane(String paneName) throws Exception {
        String[] names = {"Beginner", "Refactor", "Convention", "Warning", "Error", "Fatal"};
        JTextPane area = getTextArea(paneName);
        if (area == null) {
            throw new Exception("No pane with this name: " + paneName);
        }
        JTabbedPane jtp = (JTabbedPane)(area.getParent().getParent().getParent());
        for(int i = 0; i < jtp.getTabCount(); ++i){
                jtp.setTitleAt(i, names[i] );
        }
        beginnerErrorCount = 0;
        refactorErrorCount = 0;
        conventionErrorCount = 0;
        warningErrorCount = 0;
        errorErrorCount = 0;
        fatalErrorCount = 0;

        area.setText("");
    }

    public void addIntoPane(String paneName, String text) throws Exception {
        JTextPane area = getTextArea(paneName);
        if (area == null) {
            throw new Exception("No pane with this name: " + paneName);
        }
        JTabbedPane jtp = (JTabbedPane)(area.getParent().getParent().getParent());
        switch (paneName) {
            case "B":
                beginnerErrorCount++;
                jtp.setTitleAt(0, "Beginner ("+beginnerErrorCount+")");
                break;
            case "R":
                refactorErrorCount++;
                jtp.setTitleAt(1, "Refactor ("+refactorErrorCount+")");
                break;
            case "C":
                conventionErrorCount++;
                jtp.setTitleAt(2, "Convention ("+conventionErrorCount+")");
                break;
            case "W":
                warningErrorCount++;
                jtp.setTitleAt(3, "Warning ("+warningErrorCount+")");
                break;
            case "E":
                errorErrorCount++;
                jtp.setTitleAt(4, "Error ("+errorErrorCount+")");
                break;
            case "F":
                fatalErrorCount++;
                jtp.setTitleAt(5, "Fatal ("+fatalErrorCount+")" );
                break;
            default:

        }

        // convert line breaks to HTML line breaks
        text = text.replaceAll("\n", "<br>");
        kit.insertHTML((HTMLDocument) area.getDocument(), area.getStyledDocument().getLength(),text,0,0,null);
        //area.append(text);
    }

}
class MyHyperLinkAdapter implements HyperlinkListener{
    @Override
    public void hyperlinkUpdate(HyperlinkEvent hyperlinkEvent) {
        Desktop desktop = Desktop.getDesktop();
        if(!(hyperlinkEvent.getEventType().equals(HyperlinkEvent.EventType.ACTIVATED))){
            return;
        }
        try {
            desktop.browse(hyperlinkEvent.getURL().toURI());
        } catch (Exception x) {
            JOptionPane.showMessageDialog(null, x.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
    }
}