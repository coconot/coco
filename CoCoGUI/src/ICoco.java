/**
 * Created by dominik on 18.11.15.
 */
public interface ICoco {
    void playCoco();
    String runCoco( String filename);
    void parseCocoOutput(String line, CocoUIWindow window);
}
