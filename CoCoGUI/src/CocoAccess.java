/**
 * Created by dominik on 18.11.15.
 */

/*Jajaja coco jambo :-D Imports to play music*/

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;

import javax.swing.*;
import java.io.BufferedInputStream;
import java.io.File;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


public class CocoAccess implements ICoco {
    CoCoResourceManager crm;
    static int errorsFixedCounter = 0;

    public CocoAccess(){
        crm = new CoCoResourceManager();
    }

    @Override
    public void playCoco() {
        /*Music*/
        Thread t = new Thread(new CocoPlayMusic());
        t.start();

    }

    @Override
    public String runCoco(String filename) {
        StringBuffer sb = new StringBuffer();
        try {
            ProcessBuilder pb = new ProcessBuilder( new String[] { crm.getCoCo(), filename });
            Process p = pb.start();

            // read input
            Scanner s = new Scanner( p.getInputStream() );
            StringBuffer input = new StringBuffer();
            while(s.hasNextLine()) {
                input.append(s.nextLine()+"\n");
            }
            sb.append(input + "\n");
            s.close();

            // read error
            Scanner serr = new Scanner( p.getErrorStream() );
            String error;
            while(serr.hasNextLine()) {
                error=serr.nextLine();
                sb.append(error + "\n");
            }
            serr.close();

        }catch (Exception x){
            JOptionPane.showMessageDialog(null, x.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }
        return sb.toString();
    }

    @Override
    public void parseCocoOutput(String output, CocoUIWindow window) {

        try {
            window.clearPane("B");
            window.clearPane("R");
            window.clearPane("C");
            window.clearPane("W");
            window.clearPane("E");
            window.clearPane("F");

            // message output
            // "  5:0 E0001 invalid syntax"
            //Pattern p = Pattern.compile("\\s\\s(\\d+):(\\d)\\s(\\w)(\\d+)\\s(.+)");
            // Pattern p = Pattern.compile("^\\s\\s(\\d+):(\\d+)\\s(\\w)(\\d+)\\s(.+?)$(.*?)~~~~~~~~~~~~~~~~~~~~", Pattern.MULTILINE | Pattern.DOTALL);
            //Pattern p = Pattern.compile("^\\s\\s(\\d+):(\\d+)\\s(\\w)(\\d+)\\s(.+?)$(.+?)$(.+?)~~~~~~~~~~~~~~~~~~~~", Pattern.MULTILINE | Pattern.DOTALL);
            Pattern p = Pattern.compile("^\\s\\s(\\d+):(\\d+)\\s(\\w)(\\d+)\\s([^~]+?)$" +
                    "([^~]*?)" +  //1 group 6
                    //"(.+?)$" +  //2 group 7
                    //"(.+?)$" +  //3 group 8
                    //"(.+?)$" +  //4 group 9
                    "^\\s\\s\\s\\s(This.+?)$" +  //5 group 10
                    "(.+?)~~~~~~~~~~~~~~~~~~~~", // group 11
                    Pattern.MULTILINE | Pattern.DOTALL);
            //Pattern e_pattern = Pattern.compile("Errors fixed:\n(\\w\\d+\n):", Pattern.MULTILINE | Pattern.DOTALL);
            Pattern e_pattern = Pattern.compile("Errors fixed:\\n.*", Pattern.MULTILINE | Pattern.DOTALL);

            //Pattern p_nofix = Pattern.compile("^\\s\\s(\\d+):(\\d+)\\s(\\w)(\\d+)\\s(.+?)$(.+?)$(.+?)~~~~~~~~~~~~~~~~~~~~", Pattern.MULTILINE | Pattern.DOTALL);
            Pattern p_nofix = Pattern.compile("^\\s\\s(\\d+):(\\d+)\\s(\\w)(\\d+)\\s(.+?)$(.+?)~~~~~~~~~~~~~~~~~~~~", Pattern.MULTILINE | Pattern.DOTALL);

            Matcher me = e_pattern.matcher(output);
            if(true == me.find()){
                if(!me.group(0).isEmpty()) {
                    Pattern e_pattern2 = Pattern.compile("(\\s*\\w\\d+\\n)");
                    Matcher me2 = e_pattern2.matcher(me.group(0));
                    while(true == me2.find()){
                        errorsFixedCounter++;
                        System.out.println("errorsFixedCounter = " + errorsFixedCounter);
                    }
                }
                if(errorsFixedCounter > 0){
                    window.replayPalmTree();
                    errorsFixedCounter = 0;
                }

            }



            Matcher m = p.matcher(output);
            Matcher m_nofix = p_nofix.matcher(output);

            while(true == m.find()) {


                int colNumber = Integer.parseInt(m.group(2));

                String errType = m.group(3);
                // Beginner Checker outputs wrong line number, exactly one line too small
                int lineNrAddition = 0;
                if(errType.equals("B")){
                    System.out.println("Beginner Error Found");
                    lineNrAddition = 1;
                }
                int lineNumber = Integer.parseInt(m.group(1)) + lineNrAddition; // +1 to get the correct line number

                String errNumber = m.group(4);
                String errMessage = m.group(5);
                String fix = m.group(8);
                String errLineCode = m.group(6);
                String fixTitle = m.group(7);
                if (fix == "") {
                    fixTitle = "No previous solutions available.";
                }
                fix = fix.replace('^', ' ');
                System.out.println("1 lineNumber: " + lineNumber);
                System.out.println("2 colNumber: " + colNumber);
                System.out.println("3 errType  " + errType);
                System.out.println("4 errNumber " + errNumber);
                System.out.println("5 errMessage " + errMessage);
                System.out.println("6 errLineCode " + errLineCode);
                System.out.println("10 fixTitle " + fixTitle);
                System.out.println("11 fix " + fix);


                StringBuffer sb = new StringBuffer()
                        .append("<p>")
                        .append("<b style=\"color: #BB0000;\">"+ errMessage + " (" + errType + errNumber + ")</b>")
                        .append("<br>")
                        .append("  in line ")
                        .append(String.valueOf(lineNumber))
                        .append("<b style=\" color: #0559AD;\">" + errLineCode +  "</b>")
                        .append("<br>")
                        .append("<b style=\" color: #1F7059;\">" + fixTitle + "</b>")
                        .append("<br>")
                        .append("<b style=\" color: #1F7009;\">" + fix + "</b>")
                        .append("<br>")
                        .append("<a href=\"http://stackoverflow.com/search?q=[python]+" + errMessage.replace(' ', '+').replace('"', '+') + "\" target=\"_blank\" style=\"color: #0000BB; text-decoration: underline;\">Find out more about this error</a>")
                        .append("<br>")
                        .append("<hr>")
                        .append("<br>")
                        .append("</p>");

                // add to pane
                try {
                    window.addIntoPane(errType, sb.toString());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // The layout of messages with and without previous solutions differs extensively, so that another
            // pattern matcher is necessary

            while(true == m_nofix.find()){
                int colNumber = Integer.parseInt(m_nofix.group(2));
                String errType = m_nofix.group(3);
                // Beginner Checker outputs wrong line number, exactly one line too small
                int lineNrAddition = 0;
                if(errType.equals("B")){
                    System.out.println("Beginner Error Found");
                    lineNrAddition = 1;
                }
                int lineNumber = Integer.parseInt(m_nofix.group(1)) + lineNrAddition; // +1 to get the correct line number

                String errNumber = m_nofix.group(4);
                String errMessage = m_nofix.group(5);
                String fix = m_nofix.group(6);
                //String errLineCode = m_nofix.group(6);
                String fixTitle = "No previous solutions available.";
                System.out.println("1 lineNumber: " + lineNumber);
                System.out.println("2 colNumber: " + colNumber);
                System.out.println("3 errType  " + errType);
                System.out.println("4 errNumber " + errNumber);
                System.out.println("5 errMessage " + errMessage);
                //System.out.println("6 errLineCode " + errLineCode);
                System.out.println(" fixTitle " + fixTitle);
                System.out.println("6 fix " + fix);

                // If-condition to avoid messages that fulfill both matches to be shown twice
                if (!fix.contains("This is how you fixed this error the last times:") ) {
                    System.out.println("No This is how you fixed...");

                    fix = fix.replace('^', ' ');

                    StringBuffer sb = new StringBuffer()
                            .append("<p>")
                            .append("<b style=\"color: #BB0000;\">" + errMessage + " (" + errType + errNumber + ")</b>")
                            .append("<br>")
                            .append("  in line ")
                            .append(String.valueOf(lineNumber))
                            //.append("<b style=\" color: #0559AD;\">" + errLineCode +  "</b>")
                            //.append("<br>")
                            .append("<b style=\" color: #1F7009;\">" + fix + "</b>")
                            .append("<br>")
                            .append("<b style=\" color: #1F7059;\">" + fixTitle + "</b>")
                            .append("<br>")
                            .append("<a href=\"http://stackoverflow.com/search?q=[python]+" + errMessage.replace(' ', '+').replace('"', '+') + "\" target=\"_blank\" style=\"color: #0000BB; text-decoration: underline;\">Find out more about this error</a>")
                            .append("<br>")
                            .append("<hr>")
                            .append("<br>")
                            .append("</p>");

                    // add to pane
                    try {
                        window.addIntoPane(errType, sb.toString());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

        }catch (Exception x){
            x.printStackTrace();
        }
    }

    private class CocoPlayMusic implements Runnable{
        @Override
        public void run() {
            try {
                AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(crm.getCoCoSound());
                DataLine.Info info = new DataLine.Info(Clip.class, audioInputStream.getFormat());
                System.out.println("Inside CocoPlayMusic " + info);
                Clip clip = (Clip)AudioSystem.getLine(info);
                clip.open(audioInputStream);
                clip.start();
                Thread.sleep(9000);
                clip.stop();
            }catch(Exception x){
                System.err.println(x.getMessage());
            }
        }
    }
}
